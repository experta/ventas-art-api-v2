package ar.com.experta.ventasartapi.dao.impl;

import ar.com.experta.ventasartapi.controller.response.CotizacionAltaResponse;
import ar.com.experta.ventasartapi.controller.response.CotizacionPremioResponse;
import ar.com.experta.ventasartapi.utils.ApiUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import java.math.BigDecimal;

@Repository
public class CotizarAltaDaoImpl {

    private static final Logger logger = LoggerFactory.getLogger(CotizarDaoImpl.class);
    private EntityManager entityManager;

    @Autowired
    public CotizarAltaDaoImpl(@Qualifier("artEntityManagerFactory") EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public CotizacionAltaResponse alta(CotizacionPremioResponse request) {

        logger.debug("ART - Alta llamada al package ");

        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("art.pkg_art_internet_ventas.cotizar_laborales");
        ApiUtils apiUtils = new ApiUtils();

        proc.registerStoredProcedureParameter("as_pwid", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_vendedor", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_documento", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_razon_social", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_provincia_id", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_localidad_id", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_zona", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_ciiu", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("an_masa", BigDecimal.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("an_capitas", Integer.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_grupo_economico_sn", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_vencimiento", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_cotizar_vida_mas_50_sn", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_art_sn", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_art_tipo_afil", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_art_art_actual", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_art_alta_resc", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("an_art_alic_fija_actual", BigDecimal.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("an_art_alic_var_actual", BigDecimal.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("an_art_alic_fija", BigDecimal.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("an_art_alic_variable", BigDecimal.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_art_observacion", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_scvo_sn", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("an_scvo_cuota_pc", BigDecimal.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("an_scvo_monto", BigDecimal.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("an_scvo_der_emision", BigDecimal.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_lct_sn", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("an_lct_cuota_pc", BigDecimal.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("an_lct_cuota_pc_bruto", BigDecimal.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("an_lct_monto", BigDecimal.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_cm_sn", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("an_cm_cuota_pc", BigDecimal.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("an_cm_cuota_pc_bruto", BigDecimal.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("an_cm_monto", BigDecimal.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_descuento_sn", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("an_total_mensual_bruto", BigDecimal.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("an_total_descuento_mensual", BigDecimal.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_art_comprobante_camp", String.class, ParameterMode.IN);


        proc.registerStoredProcedureParameter("an_cotizacion_id", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("p_retorno", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("p_error_msg", String.class, ParameterMode.OUT);

        proc.setParameter("as_pwid", "123");    //ver fijo
        proc.setParameter("as_vendedor",request.getProductor());
        proc.setParameter("as_documento", request.getCuit());
        proc.setParameter("as_razon_social", request.getRazonSocial());
        proc.setParameter("as_provincia_id", request.getProvincia());
        proc.setParameter("as_localidad_id", request.getLocalidad());
        proc.setParameter("as_zona", request.getZona());
        proc.setParameter("as_ciiu", request.getCiiu());
        proc.setParameter("an_masa", new BigDecimal(request.getMasaSalarial()));
        proc.setParameter("an_capitas", Integer.valueOf(request.getCapitas()));
        proc.setParameter("as_grupo_economico_sn", "N");
        proc.setParameter("as_vencimiento", request.getFechaVencimiento());
        proc.setParameter("as_cotizar_vida_mas_50_sn", "N");
        proc.setParameter("as_art_sn","S");
        proc.setParameter("as_art_tipo_afil", apiUtils.validateDisponibilidad(request.getArtDisponibilidad(), request.getArtActual(), request.getArtRescindida()));
        proc.setParameter("as_art_art_actual", request.getArtActual());
        proc.setParameter("as_art_alta_resc", request.getArtRescindida());
        proc.setParameter("an_art_alic_fija_actual", request.getAlicuotaFijaActual());
        proc.setParameter("an_art_alic_var_actual", request.getAlicuotaVariableActual());
        proc.setParameter("an_art_alic_fija", new BigDecimal(request.getAlicuotaFija()));
        proc.setParameter("an_art_alic_variable", new BigDecimal(request.getAlicuotaVariable().toString()));
        proc.setParameter("as_art_observacion", null);
        proc.setParameter("as_scvo_sn", "N");
        proc.setParameter("an_scvo_cuota_pc", new BigDecimal("0"));
        proc.setParameter("an_scvo_monto", new BigDecimal("0"));
        proc.setParameter("an_scvo_der_emision", new BigDecimal("0"));
        proc.setParameter("as_lct_sn", "N");
        proc.setParameter("an_lct_cuota_pc", new BigDecimal("0"));
        proc.setParameter("an_lct_cuota_pc_bruto", new BigDecimal("0"));
        proc.setParameter("an_lct_monto",new BigDecimal("0"));
        proc.setParameter("as_cm_sn", "N");
        proc.setParameter("an_cm_cuota_pc", new BigDecimal("0"));
        proc.setParameter("an_cm_cuota_pc_bruto", null);
        proc.setParameter("an_cm_monto", new BigDecimal("0"));
        proc.setParameter("as_descuento_sn", null);
        proc.setParameter("an_total_mensual_bruto", new BigDecimal("0"));
        proc.setParameter("an_total_descuento_mensual", new BigDecimal("0"));
        proc.setParameter("as_art_comprobante_camp", null);

        logger.info("ART - alta - campos cargados");
        proc.execute();

        logger.info("ART:  -  en alta de  respuesta: [{}] : ", proc.getOutputParameterValue("p_retorno").toString()!=null?proc.getOutputParameterValue("p_retorno"):"Hubo un problema al grabar la cotizacion");


        CotizacionAltaResponse response =  new CotizacionAltaResponse();
        response.setNroSolicitud((String) proc.getOutputParameterValue("an_cotizacion_id"));
        response.setRetorno((String) proc.getOutputParameterValue("p_retorno"));
        response.setErrorMsg((String) proc.getOutputParameterValue("p_error_msg"));

        logger.info("ART:  - Alta: " + request.getProductor());
        return response;
    }
}
