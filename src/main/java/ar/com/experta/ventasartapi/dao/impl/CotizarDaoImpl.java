package ar.com.experta.ventasartapi.dao.impl;

import ar.com.experta.ventasartapi.controller.response.CotizacionEmpresaResponse;
import ar.com.experta.ventasartapi.controller.response.CotizacionPremio;
import ar.com.experta.ventasartapi.dao.CotizarDao;
import ar.com.experta.ventasartapi.models.CotizarArt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import java.math.BigDecimal;


@Transactional
@Repository
public class CotizarDaoImpl extends SimpleJpaRepository<CotizarArt, String> implements CotizarDao {

    private static final Logger logger = LoggerFactory.getLogger(CotizarDaoImpl.class);
    private static final  String TIPO_AFILIACION_ALTA = "NO_NECESITA_CNO";
    private static final  String TIPO_AFILIACION_TRASPASO = "CNO_NO_SOLICITADO";

    private EntityManager entityManager;

    @Autowired
    public CotizarDaoImpl(@Qualifier("artEntityManagerFactory") EntityManager entityManager) {
        super(CotizarArt.class, entityManager);
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public CotizacionEmpresaResponse empresaCheck(String cuit) {
        logger.info("ART: - buscando empresa: " + cuit);

        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("art.pkg_art_internet_ventas.empresa_info");

        proc.registerStoredProcedureParameter("as_cuit", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_registrado_afip", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_cliente_experta_art", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_cliente_experta_scvo", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_razon", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_ciiu", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_art_actual", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_art_disponibilidad", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_art_adhesion", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_permanencia_minima", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_omision_pago", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_rescindida", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_masa", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_empleados", Integer.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_provincia", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_provincia_desc", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_localidad", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_localidad_desc", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_art_zona", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("p_retorno", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("p_error_msg", String.class, ParameterMode.OUT);

        proc.setParameter("as_cuit", cuit);

        logger.info("ART - Ejecutando procedure informacion de empresa");
        proc.execute();

        logger.info("ART - Ejecutado correctamente procedure informacion de empresa");
        CotizacionEmpresaResponse response = new CotizacionEmpresaResponse();

        response.setRegistradoAfip((String) proc.getOutputParameterValue("as_registrado_afip"));
        response.setClienteExpertaArt((String) proc.getOutputParameterValue("as_cliente_experta_art"));
        response.setClienteExpertaSCVO((String) proc.getOutputParameterValue("as_cliente_experta_scvo"));
        response.setRazonSocial((String) proc.getOutputParameterValue("as_razon"));
        response.setCiiu((String) proc.getOutputParameterValue("as_ciiu"));
        response.setArtActual((String) proc.getOutputParameterValue("as_art_actual"));
        response.setArtDisponibilidad((String) proc.getOutputParameterValue("as_art_disponibilidad"));
        response.setArtAdhesion((String) proc.getOutputParameterValue("as_art_adhesion"));
        response.setArtPermanenciaMin((String) proc.getOutputParameterValue("as_permanencia_minima"));
        response.setArtOmisionPago((String) proc.getOutputParameterValue("as_omision_pago"));
        response.setArtRescindida((String) proc.getOutputParameterValue("as_rescindida"));
        response.setMasaSalarial((BigDecimal) proc.getOutputParameterValue("an_masa"));
        response.setCapitas((String) proc.getOutputParameterValue("an_empleados"));
        response.setProvincia((String) proc.getOutputParameterValue("as_provincia"));
        response.setProvinciaDesc((String) proc.getOutputParameterValue("as_provincia_desc"));
        response.setLocalidad((String) proc.getOutputParameterValue("as_localidad"));
        response.setLocalidadDesc((String) proc.getOutputParameterValue("as_localidad_desc"));
        response.setZona((String) proc.getOutputParameterValue("as_art_zona"));
        response.setRetorno((String) proc.getOutputParameterValue("p_retorno"));
        response.setErrorMsg((String) proc.getOutputParameterValue("p_error_msg"));


        logger.info("LOGIN - Obteniendo usuario: " + response.getErrorMsg());
        return response;
    }

    @Override
    public CotizacionPremio getAlicuota(CotizacionEmpresaResponse request, String cuit, String masaSalarial, String capitas, String actividad, String vendedor, String origen, String alicuotaFijaActual, String alicuotaVariableActual) {
        return null;
    }


    @Override
    public void altaCotizacion() {

    }
}
