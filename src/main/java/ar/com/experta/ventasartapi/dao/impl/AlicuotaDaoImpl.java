package ar.com.experta.ventasartapi.dao.impl;

import ar.com.experta.ventasartapi.controller.response.CotizacionEmpresaResponse;
import ar.com.experta.ventasartapi.controller.response.CotizacionPremio;
import ar.com.experta.ventasartapi.utils.ApiUtils;
import org.apache.http.HttpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import java.math.BigDecimal;
import java.util.List;

@Repository
public class AlicuotaDaoImpl {

    private static final Logger logger = LoggerFactory.getLogger(CotizarDaoImpl.class);
    private EntityManager entityManager;

    @Autowired
    public AlicuotaDaoImpl(@Qualifier("artEntityManagerFactory") EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public CotizacionPremio getAlicuota(CotizacionEmpresaResponse request, String cuit, String masaSalarial, String capitas, String actividad, String perfilVendedor, String origen, String alicuotaFijaActual, String alicuotaVariableActual ) throws HttpException {

        logger.debug("ART - Ejecutando procedure obtencion de alicuota");

        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("art.pkg_art_internet_ventas.get_precio_art");
        ApiUtils apiUtils = new ApiUtils();
        CotizacionPremio response =  new CotizacionPremio();

        proc.registerStoredProcedureParameter("as_cuit", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("an_capitas", Integer.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("an_masa", BigDecimal.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_ciiu", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_tipo_afiliacion", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_rescindida", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_zona", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_perfil_vendedor", String.class, ParameterMode.IN);

        proc.registerStoredProcedureParameter("an_alic_fija", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_alic_variable", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_alic_var_minima", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_alic_var_maxima", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_vencimiento", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("p_retorno", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("p_error_msg", String.class, ParameterMode.OUT);
        try{
            proc.setParameter("as_cuit", cuit);
            proc.setParameter("an_capitas", request.getCapitas()==null?  Integer.valueOf(capitas) :
                                    request.getArtRescindida().equals("N")?Integer.valueOf(capitas) :request.getCapitas());
            proc.setParameter("an_masa", request.getMasaSalarial()==null? new BigDecimal(masaSalarial) :
                                    request.getArtRescindida().equals("N")? new BigDecimal(masaSalarial)  : request.getMasaSalarial());
            proc.setParameter("as_ciiu", actividad != null ? actividad : request.getCiiu());
            proc.setParameter("as_tipo_afiliacion",apiUtils.validateDisponibilidad(request.getArtDisponibilidad(), request.getArtActual(), request.getArtRescindida()));
            proc.setParameter("as_rescindida", request.getArtRescindida());
            proc.setParameter("as_zona", request.getZona());
            proc.setParameter("as_perfil_vendedor", perfilVendedor);

            logger.info("ART - Ejecutando procedure art.pkg_art_internet_ventas.get_precio_art");
            proc.execute();

            response.setAlicuotaFija((proc.getOutputParameterValue("an_alic_fija")).toString().replaceAll(",","."));
            response.setAlicuotaVariable(( proc.getOutputParameterValue("an_alic_variable")).toString().replaceAll(",","."));
            response.setAlicuotaVariableMinima((proc.getOutputParameterValue("an_alic_var_minima")).toString().replaceAll(",","."));
            response.setAlicuotaVariableMaxima((proc.getOutputParameterValue("an_alic_var_maxima")).toString().replaceAll(",","."));
            response.setVencimiento((String) proc.getOutputParameterValue("as_vencimiento"));

            response.setRetorno(proc.getOutputParameterValue("p_retorno").toString());
            response.setErrorMsg(proc.getOutputParameterValue("p_error_msg")!=null? proc.getOutputParameterValue("p_error_msg").toString():"");

        } catch (Exception e){
            throw  new HttpException("Error al ejecutar servicio para obtener alicuota, alguno de los datos ingresados no es correcto :" +  e.getMessage());
        }

        return response;
    }

    public String buscarPerfil(String vendedor) throws HttpException {
  try {
      Query query  = entityManager.createNativeQuery("SELECT p.ven_perfil FROM art.art_vendedores p where p.ven_vendedor = :vendedor and ven_baja is Null")
      .setParameter("vendedor", vendedor);

      List<String> list= query.getResultList();
       if(list.isEmpty()){
           throw new HttpException("El vendedor no Existe o esta dado de Baja");
       }else {
           return list.get(0);
       }
  }catch(Exception e){
      throw new HttpException(e.getMessage());
  }
    }
}
