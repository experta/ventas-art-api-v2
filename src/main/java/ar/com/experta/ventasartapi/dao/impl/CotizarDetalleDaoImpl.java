package ar.com.experta.ventasartapi.dao.impl;

import ar.com.experta.ventasartapi.controller.response.CotizacionPremioResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import java.math.BigDecimal;

@Repository
public class CotizarDetalleDaoImpl {

    private static final Logger logger = LoggerFactory.getLogger(CotizarDaoImpl.class);
    private EntityManager entityManager;

    @Autowired
    public CotizarDetalleDaoImpl(@Qualifier("artEntityManagerFactory") EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public CotizacionPremioResponse buscarDetalleCotizacion(String cotizacionId) {

        logger.debug("ART - Alta llamada al package ");
        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("art.pkg_art_internet_ventas.detalle_laborales");
        CotizacionPremioResponse response =  new CotizacionPremioResponse();

        proc.registerStoredProcedureParameter("an_cotizacion_id", Integer.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("as_usuario", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_vendedor_id", Integer.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_vendedor", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_ejecutivo", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_cuit", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_razon_social", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_tipo_persona", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_nombre_sugerido", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_apellido_sugerido", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_provincia", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_localidad", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_cp", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_ciiu", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_ciiu_desc", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_masa", Integer.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_capitas", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_adhesion", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_disponibilidad", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_grupo_economico_sn", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_vencimiento", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_art_sn", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_art_tipo_afil", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_art_art_actual", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_art_alic_fija_actual", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_art_alic_var_actual", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_art_alic_fija", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_art_alic_variable", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_art_cuota_pc", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_art_cuota_mensual", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_art_cuota_anual", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_art_ahorro_competencia", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_art_observacion", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_art_estado", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_scvo_sn", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_scvo_cuota_pc", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_scvo_cuota_mensual", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_scvo_cuota_anual", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_scvo_monto", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_scvo_der_emision", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_scvo_fecha_inicio", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_scvo_estado", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_scvo_error", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_lct_sn", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_lct_cuota_pc", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_lct_cuota_mensual", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_lct_cuota_anual", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_lct_cuota_pc_bruto", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_lct_cuota_mensual_bruto", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_lct_cuota_anual_bruto", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_lct_monto", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_lct_estado", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_lct_error", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_cm_sn", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_cm_cuota_pc", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_cm_cuota_mensual", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_cm_cuota_anual", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_cm_cuota_pc_bruto", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_cm_cuota_mensual_bruto", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_cm_cuota_anual_bruto", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_cm_monto", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_cm_estado", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_cm_error", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_total_cuota_pc", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_total_cuota_mensual", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_total_cuota_anual", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("as_descuento_sn", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_total_bruto_mensual", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_total_descuento_mensual", BigDecimal.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("an_cantidad_productos", Integer.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("p_retorno", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("p_error_msg", String.class, ParameterMode.OUT);

        try{
            proc.setParameter("an_cotizacion_id", Integer.valueOf(cotizacionId));

            logger.info("ART - alta - campos cargados");
            proc.execute();

            logger.info("ART:  -  en alta de  respuesta: [{}] : ", proc.getOutputParameterValue("p_retorno").toString()!=null?proc.getOutputParameterValue("p_retorno"):"Hubo un problema al grabar la cotizacion");

            response.setCuit(proc.getOutputParameterValue("as_cuit").toString());
            response.setVendedor ((Integer)proc.getOutputParameterValue("as_vendedor_id"));
            response.setProductor(proc.getOutputParameterValue("as_vendedor_id").toString());
            response.setClienteExpertaArt((String) proc.getOutputParameterValue("as_art_sn"));
            response.setClienteExpertaSCVO((String) proc.getOutputParameterValue("as_scvo_sn"));
            response.setRazonSocial((String) proc.getOutputParameterValue("as_razon_social"));
            response.setCiiu((String) proc.getOutputParameterValue("as_ciiu"));
            response.setCiiuDesc((String) proc.getOutputParameterValue("as_ciiu_desc"));
            response.setArtActual((String) proc.getOutputParameterValue("as_art_art_actual"));
            response.setArtDisponibilidad(proc.getOutputParameterValue("as_disponibilidad") !=null?(String) proc.getOutputParameterValue("as_disponibilidad"):"");
            response.setArtAdhesion((String) proc.getOutputParameterValue("as_adhesion"));
            response.setMasaSalarial( proc.getOutputParameterValue("an_masa").toString().toString());
            response.setCapitas(proc.getOutputParameterValue("an_capitas").toString());
            response.setProvincia((String) proc.getOutputParameterValue("as_provincia"));
            response.setLocalidad((String) proc.getOutputParameterValue("as_localidad"));
            response.setAlicuotaFija(proc.getOutputParameterValue("an_art_alic_fija").toString());
            response.setAlicuotaFijaActual((BigDecimal) proc.getOutputParameterValue("an_art_alic_fija_actual"));
            response.setAlicuotaVariable( proc.getOutputParameterValue("an_art_alic_variable").toString());
            response.setAlicuotaVariableActual((BigDecimal) proc.getOutputParameterValue("an_art_alic_fija_actual"));
            response.setTotalCuotaMensual((BigDecimal) proc.getOutputParameterValue("an_art_cuota_mensual"));
            response.setTotalCuotaAnual((BigDecimal) proc.getOutputParameterValue("an_art_cuota_anual"));

            response.setRetorno((String) proc.getOutputParameterValue("p_retorno"));
            response.setErrorMsg((String) proc.getOutputParameterValue("p_error_msg"));
        } catch (Exception e){
            logger.info("ART:  - ERROR: Obtener cotizacion - nro de solicitud: [{}] , algunos de los campos esta llegando null  ", cotizacionId);
            response.setRetorno("error");
            response.setErrorMsg("Error al ejecutar servicio para obtener la cotizacion: ".concat( cotizacionId));
        }

        logger.info("ART:  - Busqueda de cotizacion : [{}] : ", cotizacionId);
        return response;
    }
}
