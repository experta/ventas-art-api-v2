package ar.com.experta.ventasartapi.dao;

import ar.com.experta.ventasartapi.controller.response.CotizacionEmpresaResponse;
import ar.com.experta.ventasartapi.controller.response.CotizacionPremio;
import ar.com.experta.ventasartapi.models.CotizarArt;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;


@NoRepositoryBean
public interface CotizarDao extends CrudRepository<CotizarArt, String> {

    CotizacionEmpresaResponse empresaCheck(String cuit);
    CotizacionPremio getAlicuota(CotizacionEmpresaResponse request, String cuit, String masaSalarial, String capitas, String actividad, String vendedor, String origen, String alicuotaFijaActual, String alicuotaVariableActual );

    void altaCotizacion();
}