package ar.com.experta.ventasartapi.models;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;


@JsonInclude(JsonInclude.Include.NON_NULL)
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(
                name = "cotizar_laborales",
                procedureName = "art.PKG_ART_INTERNET_VENTAS.cotizar_laborales",
                parameters = {
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "as_cuit"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "as_registrado_afip"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "as_cliente_experta_art"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "as_cliente_experta_scvo"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "as_razon"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "as_ciiu"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "as_art_actual"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "as_art_disponibilidad"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "as_art_adhesion"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "as_permanencia_minima"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "as_omision_pago"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "as_rescindida"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = BigDecimal.class, name = "an_masa"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = Integer.class, name = "an_empleados"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "as_provincia"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "as_provincia_desc"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "as_localidad"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "as_localidad_desc"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "as_art_zona"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "p_retorno"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "p_error_msg")
                })

})

@Entity
public class CotizarArt implements Serializable {
    @Id @GeneratedValue
    private Long id;
}