package ar.com.experta.ventasartapi.controller.response;

import javax.validation.Valid;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CotizacionResponse {

    private String numeroSolicitud;

    @NotBlank(message = "Ingrese cuit")
    private String cuit;

    @NotBlank(message = "No se pudo obtener CIIU")
    private String ciiu;

    @NotBlank(message = "No se pudo obtener cuit Provisorio")
    private String cuitProvisorio;

    @NotNull(message = "La fecha de inicio de vigencia es obligatoria")
    @Future(message = "La fecha de inicio de vigencia debe ser futura")
    private LocalDate fechaInicioVigencia;

    @NotBlank(message = "Ingrese nombre de productor ")
    private String productor;

    @Valid
    @NotNull (message = "La informacion de detalle Datos informados no es correcta")
    private List<Cobertura> datosInformados;

    @Valid
    @NotNull (message = "La informacion de alicuotas no es correcta")
    private List<Cobertura> nuestrasAlicutas;

    private String     retorno ;
    private String     errorMsg ;

    public CotizacionResponse() {
        this.numeroSolicitud = "123456";
        this.cuit = "27217284258";
        this.productor = "Pepe Galleta";
        this.datosInformados = setDatosInformados();
        this.nuestrasAlicutas = setAlicuotas();
    }



    public List<Cobertura> setDatosInformados(){
        List<Cobertura> mapHardCode = new ArrayList<>();
        mapHardCode.add(new Cobertura("CODIGO POR ACTIVIDAD PRINCIPAL (CIIU)","14113"));
        mapHardCode.add(new Cobertura("DESCRIPCION CIIU","Cria de ganado bovino, excepto la realizada en cabañas y para la produccion de leche (incluye ganado)"));
        mapHardCode.add(new Cobertura("CANTIDAD DE EMPLEADOS","8"));
        mapHardCode.add(new Cobertura("MASA SALARIAL (REMUNERACION TOTAL)"," 290263.00"));
        mapHardCode.add(new Cobertura("MOTIVO AFILIACION","Alta"));
        mapHardCode.add(new Cobertura(" "," "));
        return mapHardCode;
    }

    public List<Cobertura> setAlicuotas(){
        List<Cobertura> mapHardCode = new ArrayList<>();
        mapHardCode.add(new Cobertura("ALICUOTA FIJA","0"));
        mapHardCode.add(new Cobertura("ALICUOTA VARIABLE (SOBRE MASA SALARIAL)","6.22"));
        mapHardCode.add(new Cobertura("F.F.E. IDEC. NRO 590/97","0.6"));
        mapHardCode.add(new Cobertura("DIAS DE ILT","10"));
        mapHardCode.add(new Cobertura("PREMIO MENSUAL COTIZADO","18054.36"));
        mapHardCode.add(new Cobertura("PREMIO ANUAL COTIZADO","234706.66"));
        return mapHardCode;
    }

    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public String getCuit() {
        return cuit;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    public String getCuitProvisorio() {
        return cuitProvisorio;
    }

    public void setCuitProvisorio(String cuitProvisorio) {
        this.cuitProvisorio = cuitProvisorio;
    }

    public String getProductor() {
        return productor;
    }

    public void setProductor(String productor) {
        this.productor = productor;
    }

    public List<Cobertura> getDatosInformados() {
        return datosInformados;
    }

    public void setDatosInformados(List<Cobertura> datosInformados) {
        this.datosInformados = datosInformados;
    }

    public List<Cobertura> getNuestrasAlicutas() {
        return nuestrasAlicutas;
    }

    public void setNuestrasAlicutas(List<Cobertura> nuestrasAlicutas) {
        this.nuestrasAlicutas = nuestrasAlicutas;
    }

    public LocalDate getFechaInicioVigencia() {
        return fechaInicioVigencia;
    }

    public void setFechaInicioVigencia(LocalDate fechaInicioVigencia) {
        this.fechaInicioVigencia = fechaInicioVigencia;
    }

    public String getCiiu() {        return ciiu;    }

    public void setCiiu(String ciiu) {        this.ciiu = ciiu;    }

    public String getRetorno() {        return retorno;    }

    public void setRetorno(String retorno) {        this.retorno = retorno;    }

    public String getErrorMsg() {        return errorMsg;    }

    public void setErrorMsg(String errorMsg) {        this.errorMsg = errorMsg;    }
}