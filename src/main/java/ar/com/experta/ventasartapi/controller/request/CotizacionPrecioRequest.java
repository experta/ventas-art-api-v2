package ar.com.experta.ventasartapi.controller.request;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CotizacionPrecioRequest {

    @NotBlank(message = "Ingrese el CUIT")
    private String cuit;

    @NotNull (message = "Ingrese la masa salarial")
    private String masaSalarial;

    @Valid
    @NotNull (message = "Ingresar las Capitas")
    private String capitas;

    @Valid
    @NotNull (message = "La informacion del riesgo es obligatoria")
    private String actividad;

    public CotizacionPrecioRequest() {
    }

    public CotizacionPrecioRequest(@NotBlank(message = "Ingrese el CUIT") String cuit, @NotNull(message = "Ingrese la masa salarial") String masaSalarial, @Valid @NotNull(message = "Ingresar las Capitas") String capitas, @Valid @NotNull(message = "La informacion del riesgo es obligatoria") String actividad) {
        this.cuit = cuit;
        this.masaSalarial = masaSalarial;
        this.capitas = capitas;
        this.actividad = actividad;
    }

    public String getCuit() {
        return cuit;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    public String getMasaSalarial() {
        return masaSalarial;
    }

    public void setMasaSalarial(String masaSalarial) {
        this.masaSalarial = masaSalarial;
    }

    public String getCapitas() {
        return capitas;
    }

    public void setCapitas(String capitas) {
        this.capitas = capitas;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }
}
