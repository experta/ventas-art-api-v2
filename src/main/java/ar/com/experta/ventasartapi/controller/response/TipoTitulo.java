package ar.com.experta.ventasartapi.controller.response;

import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;

@ApiModel
public enum TipoTitulo {

    INCENDIO_EDIFICIO("10"),
    GASTOS_DE_ALOJAMIENTO("20"),
    RETIRO_DE_ESCOMBRO("30"),
    RIESGOS_NATURALEA_HVCT_EDIFICIO("40"),
    INCENDIO_CONTENIDO("50"),
    ROBO_Y_O_HURTO_DE_CONTENIDO_GENERAL("60"),
    TODO_RIESGO_ELECTRO("70")
    ;


    private static Map<String, TipoTitulo> map = new HashMap<>();

    static {
        for (TipoTitulo value : TipoTitulo.values()) {
            map.put(value.codigo(), value);
        }
    }

    private String codigo;

    private TipoTitulo(final String codigo) {
        this.codigo = codigo;
    }

    public static TipoTitulo fromCodigo(final String codigo) {
        return map.get(codigo);
    }

    public String codigo() {
        return codigo;
    }

}