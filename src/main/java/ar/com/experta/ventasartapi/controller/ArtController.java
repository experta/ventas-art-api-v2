package ar.com.experta.ventasartapi.controller;

import ar.com.experta.ventasartapi.controller.response.CotizacionPremioResponse;
import ar.com.experta.ventasartapi.service.ArtService;
import ar.com.experta.ventasartapi.service.KeycloakService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.http.HttpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Validated
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/")
@Api(tags = "Ventas Art", description = "Endpoints para Cotizacion de ART")
public class ArtController {
    @Autowired
    private ArtService artService;

    @Autowired
    private KeycloakService keycloakService;

    private static final Logger logger = LoggerFactory.getLogger(ArtController.class);
    private static ObjectMapper objectMapper = new ObjectMapper();


    @GetMapping("{cuit}/{masaSalarial}/{capitas}/{actividad}/{vendedor}/{origen}/{alicuotaFijaActual}/{alicuotaVariableActual}")
    @ApiOperation(value = "Cotizacion de ART")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Cotizacion exitosa"),
            @ApiResponse(code = 202, message = "Cotizacion pendiente"),
            @ApiResponse(code = 400, message = "Cotizacion petición"),
            @ApiResponse(code = 404, message = "Recurso no encontrado"),
            @ApiResponse(code = 502, message = "Servicio no disponible")})
    @ResponseBody
    public ResponseEntity<CotizacionPremioResponse> cotizacionAlicuotas(@PathVariable String cuit, @PathVariable String masaSalarial, @PathVariable String capitas, @PathVariable String actividad, @PathVariable String vendedor, @PathVariable String origen, @PathVariable String alicuotaFijaActual, @PathVariable String alicuotaVariableActual
            , @RequestHeader("Authorization") String authHeader) throws HttpException {

        try{
            this.keycloakService.validateJwt(authHeader.replace("Bearer", "").trim());
        }catch (Exception e){
            return new ResponseEntity(e.getMessage(),HttpStatus.FORBIDDEN);
        }
        CotizacionPremioResponse cotizacionPremioResponse = artService.obtenerAlicuotaArt(cuit, masaSalarial, capitas, actividad, vendedor, origen, alicuotaFijaActual, alicuotaVariableActual);
        return ResponseEntity.ok(cotizacionPremioResponse);
    }

    @RequestMapping(value = "/art/cotizacion", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Solicitar la cotizacion de una nueva póliza")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Solicitud creada"),
            @ApiResponse(code = 400, message = "Errónea petición"),
            @ApiResponse(code = 502, message = "Servicio no disponible")})
    public ResponseEntity<CotizacionPremioResponse> cotizacion (@Valid @RequestBody CotizacionPremioResponse request,
            @RequestHeader("Authorization") String authHeader) throws HttpException, JsonProcessingException {

        try {
            this.keycloakService.validateJwt(authHeader.replace("Bearer", "").trim());
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.FORBIDDEN);
        }
        logger.info("ART Cotizar: " + objectMapper.writeValueAsString(request));

     try {
         CotizacionPremioResponse response = artService.cotizarArt(request);
         return ResponseEntity.ok(response);
     }catch(Exception e){
         return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
     }
    }


    @GetMapping("/art/cotizacion/{cotizacionId}")
    @ApiOperation(value = "Cotizacion de ART")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Cotizacion exitosa"),
            @ApiResponse(code = 202, message = "Cotizacion pendiente"),
            @ApiResponse(code = 400, message = "Cotizacion petición"),
            @ApiResponse(code = 404, message = "Recurso no encontrado"),
            @ApiResponse(code = 502, message = "Servicio no disponible")})
    @ResponseBody
    public ResponseEntity<CotizacionPremioResponse> cotizacionDetalle (@PathVariable String
    cotizacionId, @RequestHeader("Authorization") String authHeader) throws HttpException, JsonProcessingException {
        try {
            this.keycloakService.validateJwt(authHeader.replace("Bearer", "").trim());
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.FORBIDDEN);
        }
        logger.info("ART Cotizar: " + objectMapper.writeValueAsString(cotizacionId));

        CotizacionPremioResponse cotizacionPremioResponse = new CotizacionPremioResponse();
        try {
            cotizacionPremioResponse = artService.cotizarDetalle(cotizacionId);
        } catch (Exception e) {
               return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(cotizacionPremioResponse);
    }


}
