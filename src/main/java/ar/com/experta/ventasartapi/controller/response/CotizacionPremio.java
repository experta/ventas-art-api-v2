package ar.com.experta.ventasartapi.controller.response;

import javax.validation.constraints.NotBlank;

public class CotizacionPremio {

    @NotBlank(message = "Alicuota Fija no encontrada")
    private String alicuotaFija;

    @NotBlank(message = "Alicuota variable no encontrada")
    private String alicuotaVariable;

    @NotBlank(message = "Alicuota variable minima no encontrada")
    private String alicuotaVariableMinima;

    @NotBlank(message = "Alicuota variable maxima no encontrada")
    private String alicuotaVariableMaxima;

    @NotBlank(message = "Vencimiento no encontrada")
    private String vencimiento;

    private String retorno ;
    private String errorMsg;

    public CotizacionPremio() {
    }

    public CotizacionPremio(@NotBlank(message = "Alicuota Fija no encontrada") String alicuotaFija, @NotBlank(message = "Alicuota variable no encontrada") String alicuotaVariable, @NotBlank(message = "Alicuota variable minima no encontrada") String alicuotaVariableMinima, @NotBlank(message = "Alicuota variable maxima no encontrada") String alicuotaVariableMaxima, @NotBlank(message = "Vencimiento no encontrada") String vencimiento, String retorno, String errorMsg) {
        this.alicuotaFija = alicuotaFija;
        this.alicuotaVariable = alicuotaVariable;
        this.alicuotaVariableMinima = alicuotaVariableMinima;
        this.alicuotaVariableMaxima = alicuotaVariableMaxima;
        this.vencimiento = vencimiento;
        this.retorno = retorno;
        this.errorMsg = errorMsg;
    }

    public String getAlicuotaFija() {
        return alicuotaFija;
    }

    public void setAlicuotaFija(String alicuotaFija) {
        this.alicuotaFija = alicuotaFija;
    }

    public String getAlicuotaVariable() {
        return alicuotaVariable;
    }

    public void setAlicuotaVariable(String alicuotaVariable) {
        this.alicuotaVariable = alicuotaVariable;
    }

    public String getAlicuotaVariableMinima() {
        return alicuotaVariableMinima;
    }

    public void setAlicuotaVariableMinima(String alicuotaVariableMinima) {
        this.alicuotaVariableMinima = alicuotaVariableMinima;
    }

    public String getAlicuotaVariableMaxima() {
        return alicuotaVariableMaxima;
    }

    public void setAlicuotaVariableMaxima(String alicuotaVariableMaxima) {
        this.alicuotaVariableMaxima = alicuotaVariableMaxima;
    }

    public String getVencimiento() {
        return vencimiento;
    }

    public void setVencimiento(String vencimiento) {
        this.vencimiento = vencimiento;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
