package ar.com.experta.ventasartapi.controller.response;

import javax.validation.constraints.NotNull;

public class CotizacionAltaResponse {
    @NotNull(message = "La informacion de alicuotas no es correcta")
    private String nroSolicitud;

    private String     retorno ;
    private String     errorMsg ;

    public CotizacionAltaResponse() {
    }

    public CotizacionAltaResponse(@NotNull(message = "La informacion de alicuotas no es correcta") String nroSolicitud, String retorno, String errorMsg) {
        this.nroSolicitud = nroSolicitud;
        this.retorno = retorno;
        this.errorMsg = errorMsg;
    }

    public String getNroSolicitud() {
        return nroSolicitud;
    }

    public void setNroSolicitud(String nroSolicitud) {
        this.nroSolicitud = nroSolicitud;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
