package ar.com.experta.ventasartapi.controller.response;

import org.springframework.format.annotation.NumberFormat;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CotizacionPremioResponse {

    @NotBlank(message = "Ingrese cuit")
    private String cuit;

    private String registradoAfip;

    private String clienteExpertaArt;

    private String clienteExpertaSCVO;

    private String razonSocial;

    @NotBlank(message = "No se pudo obtener CIIU")
    private String ciiu;

    private String ciiuDesc;

    private String artActual;

    private String artDisponibilidad;

    private String artAdhesion;

    private String artPermanenciaMin;

    private String artOmisionPago;

    private String artRescindida;

    @NotBlank(message = "No se pudo obtener Masa Salarial")
    private String masaSalarial;

    @NotBlank(message = "No se pudo obtener Capitas ")
    private String capitas;

    @NotBlank(message = "No se pudo obtener Provincia")
    private String provincia;

    private String provinciaDesc;

    @NotBlank(message = "No se pudo obtener Localidad")
    private String localidad;

    private String localidadDesc;

    @NotBlank(message = "No se pudo obtener Zona")
    private String zona;

    @NotBlank(message = "No se pudo obtener Alicuota Fija ")
    private String alicuotaFija;

    @NotBlank(message = "No se pudo obtener Alicuota Variable ")
    private String alicuotaVariable;

    private String alicuotaVarMin;

    private String alicuotaVarMax;

    private  String fechaVencimiento;

    @NotBlank(message = "Ingrese nombre de productor ")
    private String productor;

    @Valid
    private Integer vendedor;


    private List<Cobertura> datosInformados;


    private List<Cobertura> nuestrasAlicutas;

    @NumberFormat(style=NumberFormat.Style.CURRENCY)
    private BigDecimal alicuotaFijaActual;

    @NumberFormat(style=NumberFormat.Style.CURRENCY)
    private BigDecimal alicuotaVariableActual;

    private BigDecimal totalCuotaMensual;
    private BigDecimal totalCuotaAnual;

    private String fechaInicioVigencia;
    private String     numeroSolicitud ;
    private String     retorno ;
    private String     errorMsg ;

    public CotizacionPremioResponse() {

    }

    public CotizacionPremioResponse(@NotBlank(message = "Ingrese cuit") String cuit, @NotBlank(message = "Ingrese si esta registrado en Afip") String registradoAfip, @NotBlank(message = "Ingrese si la empresa es cliente de Experta ART") String clienteExpertaArt, @NotBlank(message = "Ingrese SCVO") String clienteExpertaSCVO, @NotBlank(message = "No se pudo obtener la Razon social") String razonSocial, @NotBlank(message = "No se pudo obtener CIIU") String ciiu, String ciiuDesc, @NotBlank(message = "No se pudo obtener Art actual") String artActual, @NotBlank(message = "No se pudo obtener Art Disponibilidad (CNO no solicidato)") String artDisponibilidad, @NotBlank(message = "No se pudo obtener Art adhesion") String artAdhesion, @NotBlank(message = "No se pudo obtener Art Permanencia Min") String artPermanenciaMin, @NotBlank(message = "No se pudo obtener Art Permanencia Min") String artOmisionPago, @NotBlank(message = "No se pudo obtener Art Permanencia Min") String artRescindida, @NotBlank(message = "No se pudo obtener Masa Salarial") String masaSalarial, @NotBlank(message = "No se pudo obtener Capitas ") String capitas, @NotBlank(message = "No se pudo obtener Provincia") String provincia, @NotBlank(message = "No se pudo obtener descripcion de  Provincia") String provinciaDesc, @NotBlank(message = "No se pudo obtener Localidad") String localidad, @NotBlank(message = "No se pudo obtener descripcion Localidad") String localidadDesc, @NotBlank(message = "No se pudo obtener Zona") String zona, @NotBlank(message = "No se pudo obtener Alicuota Fija ") String alicuotaFija, @NotBlank(message = "No se pudo obtener Alicuota Variable ") String alicuotaVariable, @NotBlank(message = "No se pudo obtener Alicuota variable minima ") String alicuotaVarMin, @NotBlank(message = "No se pudo obtener Alicuota variable maxima") String alicuotaVarMax, @NotNull(message = "La fecha de inicio de vigencia es obligatoria") String fechaVencimiento, @NotBlank(message = "Ingrese nombre de productor ") String productor, @Valid Integer vendedor, List<Cobertura> datosInformados, List<Cobertura> nuestrasAlicutas, BigDecimal alicuotaFijaActual, BigDecimal alicuotaVariableActual, BigDecimal totalCuotaMensual, BigDecimal totalCuotaAnual, String fechaInicioVigencia, String numeroSolicitud, String retorno, String errorMsg) {
        this.cuit = cuit;
        this.registradoAfip = registradoAfip;
        this.clienteExpertaArt = clienteExpertaArt;
        this.clienteExpertaSCVO = clienteExpertaSCVO;
        this.razonSocial = razonSocial;
        this.ciiu = ciiu;
        this.ciiuDesc = ciiuDesc;
        this.artActual = artActual;
        this.artDisponibilidad = artDisponibilidad;
        this.artAdhesion = artAdhesion;
        this.artPermanenciaMin = artPermanenciaMin;
        this.artOmisionPago = artOmisionPago;
        this.artRescindida = artRescindida;
        this.masaSalarial = masaSalarial;
        this.capitas = capitas;
        this.provincia = provincia;
        this.provinciaDesc = provinciaDesc;
        this.localidad = localidad;
        this.localidadDesc = localidadDesc;
        this.zona = zona;
        this.alicuotaFija = alicuotaFija;
        this.alicuotaVariable = alicuotaVariable;
        this.alicuotaVarMin = alicuotaVarMin;
        this.alicuotaVarMax = alicuotaVarMax;
        this.fechaVencimiento = fechaVencimiento;
        this.productor = productor;
        this.vendedor = vendedor;
        this.datosInformados = datosInformados;
        this.nuestrasAlicutas = nuestrasAlicutas;
        this.alicuotaFijaActual = alicuotaFijaActual;
        this.alicuotaVariableActual = alicuotaVariableActual;
        this.totalCuotaMensual = totalCuotaMensual;
        this.totalCuotaAnual = totalCuotaAnual;
        this.fechaInicioVigencia = fechaInicioVigencia;
        this.numeroSolicitud = numeroSolicitud;
        this.retorno = retorno;
        this.errorMsg = errorMsg;
    }

    public List<Cobertura> armarListaDatosInformados(String ciiu, String desc, String capitas, String masaSalarial, String afiliacion){
        List<Cobertura> map = new ArrayList<>();
        map.add(new Cobertura("CODIGO POR ACTIVIDAD PRINCIPAL (CIIU)",ciiu));
        map.add(new Cobertura("DESCRIPCION CIIU",desc));
        map.add(new Cobertura("CANTIDAD DE EMPLEADOS",capitas));
        map.add(new Cobertura("MASA SALARIAL (REMUNERACION TOTAL)",masaSalarial));
        map.add(new Cobertura("MOTIVO AFILIACION",afiliacion));
        map.add(new Cobertura(" "," "));
        return map;
    }

    public List<Cobertura> armarListaAlicuotas(String alicuotaFija, String alicuotaVariable, String premioMensual, String premioAnual){
        List<Cobertura> map = new ArrayList<>();
        map.add(new Cobertura("ALICUOTA FIJA",alicuotaFija));
        map.add(new Cobertura("ALICUOTA VARIABLE (SOBRE MASA SALARIAL)",alicuotaVariable));
        map.add(new Cobertura("F.F.E. IDEC. NRO 590/97","0.6"));
        map.add(new Cobertura("DIAS DE ILT","10"));
        map.add(new Cobertura("PREMIO MENSUAL COTIZADO",premioMensual));
        map.add(new Cobertura("PREMIO ANUAL COTIZADO",premioAnual));
        return map;
    }


    public String getCuit() {
        return cuit;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    public String getRegistradoAfip() {
        return registradoAfip;
    }

    public void setRegistradoAfip(String registradoAfip) {
        this.registradoAfip = registradoAfip;
    }

    public String getClienteExpertaArt() {
        return clienteExpertaArt;
    }

    public void setClienteExpertaArt(String clienteExpertaArt) {
        this.clienteExpertaArt = clienteExpertaArt;
    }

    public String getClienteExpertaSCVO() {
        return clienteExpertaSCVO;
    }

    public void setClienteExpertaSCVO(String clienteExpertaSCVO) {
        this.clienteExpertaSCVO = clienteExpertaSCVO;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getCiiu() {
        return ciiu;
    }

    public void setCiiu(String ciiu) {
        this.ciiu = ciiu;
    }

    public String getCiiuDesc() {
        return ciiuDesc;
    }

    public void setCiiuDesc(String ciiuDesc) {
        this.ciiuDesc = ciiuDesc;
    }

    public String getArtActual() {
        return artActual;
    }

    public void setArtActual(String artActual) {
        this.artActual = artActual;
    }

    public String getArtDisponibilidad() {
        return artDisponibilidad;
    }

    public void setArtDisponibilidad(String artDisponibilidad) {
        this.artDisponibilidad = artDisponibilidad;
    }

    public String getArtAdhesion() {
        return artAdhesion;
    }

    public void setArtAdhesion(String artAdhesion) {
        this.artAdhesion = artAdhesion;
    }

    public String getArtPermanenciaMin() {
        return artPermanenciaMin;
    }

    public void setArtPermanenciaMin(String artPermanenciaMin) {
        this.artPermanenciaMin = artPermanenciaMin;
    }

    public String getArtOmisionPago() {
        return artOmisionPago;
    }

    public void setArtOmisionPago(String artOmisionPago) {
        this.artOmisionPago = artOmisionPago;
    }

    public String getArtRescindida() {
        return artRescindida;
    }

    public void setArtRescindida(String artRescindida) {
        this.artRescindida = artRescindida;
    }

    public String getMasaSalarial() {
        return masaSalarial;
    }

    public void setMasaSalarial(String masaSalarial) {
        this.masaSalarial = masaSalarial;
    }

    public String getCapitas() {
        return capitas;
    }

    public void setCapitas(String capitas) {
        this.capitas = capitas;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getProvinciaDesc() {
        return provinciaDesc;
    }

    public void setProvinciaDesc(String provinciaDesc) {
        this.provinciaDesc = provinciaDesc;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getLocalidadDesc() {
        return localidadDesc;
    }

    public void setLocalidadDesc(String localidadDesc) {
        this.localidadDesc = localidadDesc;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getAlicuotaFija() {
        return alicuotaFija;
    }

    public void setAlicuotaFija(String alicuotaFija) {
        this.alicuotaFija = alicuotaFija;
    }

    public String getAlicuotaVariable() {
        return alicuotaVariable;
    }

    public void setAlicuotaVariable(String alicuotaVariable) {
        this.alicuotaVariable = alicuotaVariable;
    }

    public String getAlicuotaVarMin() {
        return alicuotaVarMin;
    }

    public void setAlicuotaVarMin(String alicuotaVarMin) {
        this.alicuotaVarMin = alicuotaVarMin;
    }

    public String getAlicuotaVarMax() {
        return alicuotaVarMax;
    }

    public void setAlicuotaVarMax(String alicuotaVarMax) {
        this.alicuotaVarMax = alicuotaVarMax;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getProductor() {
        return productor;
    }

    public void setProductor(String productor) {
        this.productor = productor;
    }

    public Integer getVendedor() {
        return vendedor;
    }

    public void setVendedor(Integer vendedor) {
        this.vendedor = vendedor;
    }

    public List<Cobertura> getDatosInformados() {
        return datosInformados;
    }

    public void setDatosInformados(List<Cobertura> datosInformados) {
        this.datosInformados = datosInformados;
    }

    public List<Cobertura> getNuestrasAlicutas() {
        return nuestrasAlicutas;
    }

    public void setNuestrasAlicutas(List<Cobertura> nuestrasAlicutas) {
        this.nuestrasAlicutas = nuestrasAlicutas;
    }

    public String getFechaInicioVigencia() {return fechaInicioVigencia; }

    public void setFechaInicioVigencia(String fechaInicioVigencia) { this.fechaInicioVigencia = fechaInicioVigencia;}

    public String getNumeroSolicitud() {return numeroSolicitud; }

    public void setNumeroSolicitud(String numeroSolicitud) {this.numeroSolicitud = numeroSolicitud; }

    public BigDecimal getAlicuotaFijaActual() {
        return alicuotaFijaActual;
    }

    public void setAlicuotaFijaActual(BigDecimal alicuotaFijaActual) {
        this.alicuotaFijaActual = alicuotaFijaActual;
    }

    public BigDecimal getAlicuotaVariableActual() {
        return alicuotaVariableActual;
    }

    public void setAlicuotaVariableActual(BigDecimal alicuotaVariableActual) {
        this.alicuotaVariableActual = alicuotaVariableActual;
    }

    public BigDecimal getTotalCuotaMensual() {
        return totalCuotaMensual;
    }

    public void setTotalCuotaMensual(BigDecimal totalCuotaMensual) {
        this.totalCuotaMensual = totalCuotaMensual;
    }

    public BigDecimal getTotalCuotaAnual() {
        return totalCuotaAnual;
    }

    public void setTotalCuotaAnual(BigDecimal totalCuotaAnual) {
        this.totalCuotaAnual = totalCuotaAnual;
    }

    public String getRetorno() {

        return retorno;
    }

    public void setRetorno(String retorno) {

        this.retorno = retorno;
    }

    public String getErrorMsg() {

        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {

        this.errorMsg = errorMsg;
    }
}
