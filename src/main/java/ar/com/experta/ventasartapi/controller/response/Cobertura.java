package ar.com.experta.ventasartapi.controller.response;

import javax.validation.constraints.NotNull;

public class Cobertura {

    @NotNull (message = "Es necesario ingresar el titulo de datos o alicuotas")
    private String titulo;
    @NotNull (message = "Ingrese el detalle del dato informado o alicuota")
    private String dato;

    public Cobertura() {
    }

    public Cobertura(@NotNull(message = "Ingrese tipo de cobertura") String titulo, @NotNull(message = "Ingrese suma asegurada") String dato) {
        this.titulo = titulo;
        this.dato = dato;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDato() {
        return dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }
}

