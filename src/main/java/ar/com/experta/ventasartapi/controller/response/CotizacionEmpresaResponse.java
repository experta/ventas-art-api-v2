package ar.com.experta.ventasartapi.controller.response;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class CotizacionEmpresaResponse {

    @NotNull(message = "Ingrese si la empresa esta registrada en AFIP")
    private String registradoAfip;

    @NotNull(message = "Ingrese si es cliente Experta Art")
    private String clienteExpertaArt;

    @NotNull(message = "Ingrese si es cliente Experta SCVO")
    private String clienteExpertaSCVO;

    @NotBlank(message = "Ingrese Razon Social")
    private String razonSocial;

    @NotBlank(message = "Ingrese CIIU")
    private String ciiu;

    @NotBlank(message = "Ingrese art actual ")
    private String artActual;

    @NotBlank(message = "Ingrese art Disponibilidad ")
    private String artDisponibilidad;

    @NotBlank(message = "Ingrese art Adhesion ")
    private String artAdhesion;

    @NotBlank(message = "Ingrese art permanenciaMin")
    private String artPermanenciaMin;

    @NotBlank(message = "Ingrese art omision pago")
    private String artOmisionPago;

    @NotBlank(message = "Ingrese art rescindida")
    private String artRescindida;

    @NotNull (message = "Ingrese la masa salarial")
    private BigDecimal masaSalarial;

    @Valid
    @NotNull (message = "Ingresar las Capitas")
    private String capitas;


    @NotBlank(message = "Ingrese Provicia")
    private String provincia;

    @NotBlank(message = "Ingrese Provicia Descripcion")
    private String provinciaDesc;

    @NotBlank(message = "Ingrese Localidad")
    private String localidad;

    @NotBlank(message = "Ingrese Localidad desc")
    private String localidadDesc;

    @NotBlank(message = "Ingrese Zona")
    private String zona;

    private String     retorno ;
    private String     errorMsg ;

    public CotizacionEmpresaResponse() {

    }

    public CotizacionEmpresaResponse(@NotNull(message = "Ingrese si la empresa esta registrada en AFIP") String registradoAfip, @NotNull(message = "Ingrese si es cliente Experta Art") String clienteExpertaArt, @NotNull(message = "Ingrese si es cliente Experta SCVO") String clienteExpertaScvo, @NotBlank(message = "Ingrese Razon Social") String razonSocial, @NotBlank(message = "Ingrese CIIU") String ciiu, @NotBlank(message = "Ingrese art actual ") String artActual, @NotBlank(message = "Ingrese art Disponibilidad ") String artDisponibilidad, @NotBlank(message = "Ingrese art Adhesion ") String artAdhesion, @NotBlank(message = "Ingrese art permanenciaMin") String artPermanenciaMin, @NotBlank(message = "Ingrese art omision pago") String artOmisionPago, @NotBlank(message = "Ingrese art rescindida") String artRescindida, @NotNull(message = "Ingrese la masa salarial") BigDecimal masaSalarial, @Valid @NotNull(message = "Ingresar las Capitas") String capitas, @NotBlank(message = "Ingrese Provicia") String provincia, @NotBlank(message = "Ingrese Provicia Descripcion") String provinciaDesc, @NotBlank(message = "Ingrese Localidad") String localidad, @NotBlank(message = "Ingrese Localidad") String localidadDesc, @NotBlank(message = "Ingrese Zona") String zona, String retorno, String errorMsg) {
        this.registradoAfip = registradoAfip;
        this.clienteExpertaArt = clienteExpertaArt;
        this.clienteExpertaSCVO = clienteExpertaScvo;
        this.razonSocial = razonSocial;
        this.ciiu = ciiu;
        this.artActual = artActual;
        this.artDisponibilidad = artDisponibilidad;
        this.artAdhesion = artAdhesion;
        this.artPermanenciaMin = artPermanenciaMin;
        this.artOmisionPago = artOmisionPago;
        this.artRescindida = artRescindida;
        this.masaSalarial = masaSalarial;
        this.capitas = capitas;
        this.provincia = provincia;
        this.provinciaDesc = provinciaDesc;
        this.localidad = localidad;
        this.localidadDesc = localidadDesc;
        this.zona = zona;
        this.retorno = retorno;
        this.errorMsg = errorMsg;
    }

    public String getRegistradoAfip() {
        return registradoAfip;
    }

    public void setRegistradoAfip(String registradoAfip) {   this.registradoAfip = registradoAfip; }

    public String getClienteExpertaArt() {
        return clienteExpertaArt;
    }

    public void setClienteExpertaArt(String clienteExpertaArt) {
        this.clienteExpertaArt = clienteExpertaArt;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {    this.razonSocial = razonSocial;}

    public String getCiiu() {
        return ciiu;
    }

    public void setCiiu(String ciiu) {
        this.ciiu = ciiu;
    }

    public String getArtActual() {
        return artActual;
    }

    public void setArtActual(String artActual) {
        this.artActual = artActual;
    }

    public String getArtDisponibilidad() {
        return artDisponibilidad;
    }

    public void setArtDisponibilidad(String artDisponibilidad) {
        this.artDisponibilidad = artDisponibilidad;
    }

    public String getArtAdhesion() {    return artAdhesion; }

    public void setArtAdhesion(String artAdhesion) {
        this.artAdhesion = artAdhesion;
    }

    public String getArtPermanenciaMin() {
        return artPermanenciaMin;
    }

    public void setArtPermanenciaMin(String artPermanenciaMin) {
        this.artPermanenciaMin = artPermanenciaMin;
    }

    public String getArtOmisionPago() {
        return artOmisionPago;
    }

    public void setArtOmisionPago(String artOmisionPago) {
        this.artOmisionPago = artOmisionPago;
    }

    public String getArtRescindida() {
        return artRescindida;
    }

    public void setArtRescindida(String artRescindida) {
        this.artRescindida = artRescindida;
    }

    public BigDecimal getMasaSalarial() {
        return masaSalarial;
    }

    public void setMasaSalarial(BigDecimal masaSalarial) {
        this.masaSalarial = masaSalarial;
    }

    public String getCapitas() {
        return capitas;
    }

    public void setCapitas(String capitas) {
        this.capitas = capitas;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getProvinciaDesc() {
        return provinciaDesc;
    }

    public void setProvinciaDesc(String provinciaDesc) {
        this.provinciaDesc = provinciaDesc;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getClienteExpertaSCVO() { return clienteExpertaSCVO;    }

    public void setClienteExpertaSCVO(String clienteExpertaSCVO) { this.clienteExpertaSCVO = clienteExpertaSCVO; }

    public String getLocalidadDesc() { return localidadDesc;    }

    public void setLocalidadDesc(String localidadDesc) {this.localidadDesc = localidadDesc;   }

    public String getRetorno() { return retorno;    }

    public void setRetorno(String retorno) {   this.retorno = retorno;    }

    public String getErrorMsg() { return errorMsg;    }

    public void setErrorMsg(String errorMsg) {this.errorMsg = errorMsg;    }
}
