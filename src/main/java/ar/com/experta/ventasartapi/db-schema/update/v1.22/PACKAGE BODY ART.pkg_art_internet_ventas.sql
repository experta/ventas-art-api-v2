CREATE OR REPLACE PACKAGE BODY ART.pkg_art_internet_ventas IS

PROCEDURE cotizar_laborales( as_pwid                    IN VARCHAR2,
                              as_vendedor                IN VARCHAR2,
                              as_documento               IN VARCHAR2,
                              as_razon_social            IN VARCHAR2,
                              as_provincia_id            IN VARCHAR2,
                              as_localidad_id            IN VARCHAR2,
                              as_zona                    IN VARCHAR2,
                              as_ciiu                    IN VARCHAR2,
                              an_masa                    IN NUMBER,
                              an_capitas                 IN NUMBER,
                              as_grupo_economico_sn      IN VARCHAR2,
                              as_vencimiento             IN VARCHAR2,
                              as_cotizar_vida_mas_50_sn  IN VARCHAR2,
                              as_art_sn                  IN VARCHAR2,
                              as_art_tipo_afil           IN VARCHAR2,
                              as_art_art_actual          IN VARCHAR2,
                              as_art_alta_resc           IN VARCHAR2,
                              an_art_alic_fija_actual    IN NUMBER,
                              an_art_alic_var_actual     IN NUMBER,
                              an_art_alic_fija           IN NUMBER,
                              an_art_alic_variable       IN NUMBER,
                              as_art_observacion         IN VARCHAR2,
                              as_scvo_sn                 IN VARCHAR2,
                              an_scvo_cuota_pc           IN NUMBER,
                              an_scvo_monto              IN NUMBER,
                              an_scvo_der_emision        IN NUMBER,
                              as_lct_sn                  IN VARCHAR2,
                              an_lct_cuota_pc            IN NUMBER,
                              an_lct_cuota_pc_bruto      IN NUMBER,
                              an_lct_monto               IN NUMBER,
                              as_cm_sn                   IN VARCHAR2,
                              an_cm_cuota_pc             IN NUMBER,
                              an_cm_cuota_pc_bruto       IN NUMBER,
                              an_cm_monto                IN NUMBER,
                              as_descuento_sn            IN VARCHAR2,
                              an_total_mensual_bruto     IN NUMBER,
                              an_total_descuento_mensual IN NUMBER,

                              an_cotizacion_id OUT VARCHAR2,
                              p_retorno        OUT VARCHAR2,
                              p_error_msg      OUT VARCHAR2); IS
  
  BEGIN

    pkg_art_log.grabar(as_texto     => pkg_internet_constantes.error ||
                                         substr(error_msg, 1, 3900),
                         as_procedure => 'pkg_art_internet_ventas.cotizar_laborales',
                         an_linea     => $$pl_sql_line,
                         as_pb_or     => 'OR');

  
  EXCEPTION
    WHEN OTHERS THEN
      ERROR_CODE := pkg_internet_constantes.errors_to_front_end;
      error_msg  := f_sqlerrm;
      pkg_art_log.grabar(as_texto     => pkg_internet_constantes.error ||
                                         substr(error_msg, 1, 3900),
                         as_procedure => 'pkg_art_mobile.solicitud_empresa_info',
                         an_linea     => $$pl_sql_line,
                         as_pb_or     => 'OR');
  END cotizar_laborales;

END pkg_art_internet_ventas;
