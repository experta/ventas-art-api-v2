package ar.com.experta.ventasartapi.exceptions;

import java.util.List;

public class DebitoAutomaticoErrorException extends NegocioException {
    public DebitoAutomaticoErrorException() {
        super("Si el medio de pago es debito automatico, la informacion de debito automatico CBU es obligatoria");
    }

    public List<String> getErrorList() {
        return null;
    }
}
