package ar.com.experta.ventasartapi.exceptions;

import java.util.List;

public class MonedaCanjeErrorException extends NegocioException {

    public MonedaCanjeErrorException() {
        super("Si la forma de pago es canje la moneda debe ser quintales");
    }

    @Override
    public List<String> getErrorList() {
        return null;
    }
}
