package ar.com.experta.ventasartapi.exceptions;

public class UnauthorizedException extends RuntimeException{

    private int code;
    private final String message;

    public UnauthorizedException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public UnauthorizedException(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}