package ar.com.experta.ventasartapi.exceptions;

import java.util.List;

public class TarjetaCreditoErrorException extends NegocioException {

    public TarjetaCreditoErrorException() {
        super("Si el medio de pago es tarjeta de credito, la informacion de la tarjeta de credito es obligatoria");
    }

    public List<String> getErrorList() {
        return null;
    }
}
