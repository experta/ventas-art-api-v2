package ar.com.experta.ventasartapi.exceptions;

import java.util.List;

public class ErrorMessagePersonalizado {
    private int status;
    private String error;
    private List<String> errors;
    private String message;

    public ErrorMessagePersonalizado(int status, String error, String message) {
        this.status = status;
        this.error = error;
        this.message = message;
    }

    public ErrorMessagePersonalizado(int status, String error, List<String> errors, String message) {
        this.status = status;
        this.error = error;
        this.errors = errors;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}