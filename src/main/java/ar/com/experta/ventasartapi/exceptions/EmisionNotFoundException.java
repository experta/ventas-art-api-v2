package ar.com.experta.ventasartapi.exceptions;

public class EmisionNotFoundException extends RuntimeException {
    public EmisionNotFoundException() {
        super("La emision buscada no existe");
    }
}
