package ar.com.experta.ventasartapi.exceptions;

import java.util.List;

public class EmisionException extends NegocioException {

    private List<String> errorList;

    public EmisionException(List<String> errorList) {
        super();
        this.errorList = errorList;
    }

    public List<String> getErrorList() {
        return errorList;
    }
}
