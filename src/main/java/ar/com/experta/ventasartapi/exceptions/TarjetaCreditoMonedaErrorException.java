package ar.com.experta.ventasartapi.exceptions;

import java.util.List;

public class TarjetaCreditoMonedaErrorException extends NegocioException {

    public TarjetaCreditoMonedaErrorException() {
        super("Si el medio de pago es tarjeta de credito, la moneda debe ser PESOS");
    }

    public List<String> getErrorList() {
        return null;
    }


}
