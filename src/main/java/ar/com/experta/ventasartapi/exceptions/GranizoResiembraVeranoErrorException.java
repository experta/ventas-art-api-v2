package ar.com.experta.ventasartapi.exceptions;

import java.util.List;

public class GranizoResiembraVeranoErrorException extends NegocioException {

    public GranizoResiembraVeranoErrorException() {
        super("La cobertura de granizo resiembra del 40% solo es permitida para plan tradicional y no verano");
    }

    public List<String> getErrorList() {
        return null;
    }
}