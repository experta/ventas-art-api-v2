package ar.com.experta.ventasartapi.exceptions;

public class ParseException extends BadGatewayException {
    public ParseException() {
        super("Ha ocurrido un error parseando la respuesta");
    }

}
