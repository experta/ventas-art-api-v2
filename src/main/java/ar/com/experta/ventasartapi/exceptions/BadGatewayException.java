package ar.com.experta.ventasartapi.exceptions;

public class BadGatewayException extends RuntimeException{
    public BadGatewayException(String message) {
        super(message);
    }

    public BadGatewayException() {
    }

}
