package ar.com.experta.ventasartapi.exceptions;


public class PolizaPDFNotFoundException extends RuntimeException {
    public PolizaPDFNotFoundException() {
        super("La poliza buscada es inexistente");
    }
}
