package ar.com.experta.ventasartapi.exceptions;

import java.util.List;

public class CotizacionException extends NegocioException {
    private List<String> errorList;

    public CotizacionException(List<String> errorList) {
        super();
        this.errorList = errorList;
    }

    public List<String> getErrorList() {
        return errorList;
    }
}
