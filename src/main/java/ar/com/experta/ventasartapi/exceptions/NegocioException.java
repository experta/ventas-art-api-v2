package ar.com.experta.ventasartapi.exceptions;

import java.util.List;

public abstract class NegocioException extends RuntimeException{

    public NegocioException() {
    }

    public NegocioException(String message) {
        super(message);
    }

    public abstract List<String> getErrorList();

}
