package ar.com.experta.ventasartapi.exceptions;


import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {ParseException.class, BadGatewayException.class})
    protected ResponseEntity<Object> handleBussinessBadGatewayExceptions(BadGatewayException ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorMessagePersonalizado(
                                                HttpStatus.BAD_GATEWAY.value(),
                                                HttpStatus.BAD_GATEWAY.getReasonPhrase(),
                                                Arrays.asList(ex.getMessage()),
                                                ex.getMessage()),
                                        new HttpHeaders(),
                                        HttpStatus.BAD_GATEWAY,
                                        request);
    }

    @ExceptionHandler(value = {NegocioException.class})
    protected ResponseEntity<Object> handleBadRequestExceptions(NegocioException ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorMessagePersonalizado(
                        HttpStatus.BAD_REQUEST.value(),
                        HttpStatus.BAD_REQUEST.getReasonPhrase(),
                        ex.getErrorList(),
                        ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST,
                request);
    }

    @ExceptionHandler(value = {EmisionNotFoundException.class, PolizaPDFNotFoundException.class})
    protected ResponseEntity<Object> handleNotFoundExceptions(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorMessagePersonalizado(
                        HttpStatus.NOT_FOUND.value(),
                        HttpStatus.NOT_FOUND.getReasonPhrase(),
                        Arrays.asList(ex.getMessage()),
                        ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.NOT_FOUND,
                request);
    }


    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorMessagePersonalizado(
                        HttpStatus.BAD_REQUEST.value(),
                        HttpStatus.BAD_REQUEST.getReasonPhrase(),
                        Arrays.asList(ex.getMessage()),
                        ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST,
                request);
    }

    // this code is to  @Valid when wrong request arrive
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(  MethodArgumentNotValidException ex,
                                                                    HttpHeaders headers,
                                                                    HttpStatus status,
                                                                    WebRequest request) {

        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());

        return handleExceptionInternal(ex, new ErrorMessagePersonalizado(
                        HttpStatus.BAD_REQUEST.value(),
                        HttpStatus.BAD_REQUEST.getReasonPhrase(),
                        errors,
                        ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST,
                request);
    }


}