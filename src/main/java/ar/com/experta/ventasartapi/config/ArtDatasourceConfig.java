package ar.com.experta.ventasartapi.config;

import ar.com.experta.ventasartapi.dao.impl.AlicuotaDaoImpl;
import ar.com.experta.ventasartapi.dao.impl.CotizarDaoImpl;
import ar.com.experta.ventasartapi.models.CotizarArt;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * Configuración de Datasource para base ART
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
	entityManagerFactoryRef="artEntityManagerFactory",
	basePackageClasses = {CotizarDaoImpl.class, AlicuotaDaoImpl.class}
)
public class ArtDatasourceConfig {

	@Primary
	@Bean("artDataSource")
	@ConfigurationProperties("spring.datasource")
	public DataSource artDataSource() {
		return DataSourceBuilder.create().build();
	}	


	@Primary
	@Bean(name = "artEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder,
			@Qualifier("artDataSource") DataSource dataSource) {
		Map<String, String> properties = new HashMap<>();
		properties.put("hibernate.default_schema", null);
		return builder
	      .dataSource(dataSource)
	      .packages(CotizarArt.class )
	      .persistenceUnit("art")
	      .properties(properties)
	      .build();
	}

	@Primary
	@Bean(name = "transactionManager")
	public PlatformTransactionManager transactionManager(
			@Qualifier("artEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}
	
}
