package ar.com.experta.ventasartapi.config;

public class Constantes {
    public static final  String TIPO_AFILIACION_ALTA = "NO_NECESITA_CNO";
    public static final  String TIPO_AFILIACION_TRASPASO = "CNO_NO_SOLICITADO";
    public static final  String SET_ALTA = "ALTA";
    public static final  String SET_TRASPASO = "TRASPASO";

}
