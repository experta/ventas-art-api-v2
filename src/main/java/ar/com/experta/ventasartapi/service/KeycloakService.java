package ar.com.experta.ventasartapi.service;

import ar.com.experta.ventasartapi.client.KeycloakClient;
import ar.com.experta.ventasartapi.controller.request.RequestKeycloakLogin;
import ar.com.experta.ventasartapi.controller.response.LoginResponse;
import ar.com.experta.ventasartapi.dto.UsuarioArtApi;
import ar.com.experta.ventasartapi.dto.UsuarioKeycloak;
import ar.com.experta.ventasartapi.exceptions.BadGatewayException;
import ar.com.experta.ventasartapi.exceptions.UnauthorizedException;
import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.http.HttpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KeycloakService {

    @Autowired
    private KeycloakClient keycloakClient;

    private UsuarioArtApi  usuarioArtApi;
    private static final String RAMO_HOGAR = "HOGAR";
    private static final String RAMO_AP = "AP";


    public LoginResponse login(RequestKeycloakLogin requestKeycloakLogin) {


        UsuarioKeycloak usuario = new UsuarioKeycloak();
        try {
            usuario = keycloakClient.login(requestKeycloakLogin);

        } catch (UnauthorizedException error) {

            throw new UnauthorizedException("d");
        } catch (BadGatewayException error) {

            throw new BadGatewayException("Hubo un error al iniciar sesión, intentá nuevamente en unos minutos.");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return new LoginResponse(usuario.getAccess_token(),usuario.getRefresh_token());
    }




    public LoginResponse refreshToken(String requestRefreshTokenKeycloak) {

        UsuarioKeycloak usuario = new UsuarioKeycloak();
        try {
            usuario = keycloakClient.refreshToken(requestRefreshTokenKeycloak);
        } catch (UnauthorizedException error) {

            throw new UnauthorizedException("Token invalido");
        } catch (BadGatewayException error) {

            throw new BadGatewayException("Hubo un error al obtener el refresh token");
        }

        return new LoginResponse(usuario.getAccess_token(),usuario.getRefresh_token());
    }

    public void validateJwt(String jwtBearer) throws HttpException {

        DecodedJWT jwt = JWT.decode(jwtBearer);

        // check JWT role is correct
        List<String> roles = ((List)jwt.getClaim("realm_access").asMap().get("roles"));
        if(!roles.contains("ROL_BROKER"))
            throw new UnauthorizedException("Unauthorized");
    }

}
