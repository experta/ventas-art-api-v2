package ar.com.experta.ventasartapi.service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class RequestKeycloakLogin {


    @Valid
    private String client_id;
    @NotNull
    @Valid
    private String username;
    @NotNull
    @Valid
    private String password;
    @Valid
    private String grant_type;
    @Valid
    private String client_secret;

    public RequestKeycloakLogin(){}

    public RequestKeycloakLogin(String client_id, String username, String password, String grant_type, String client_secret) {
        this.client_id = client_id;
        this.username = username;
        this.password = password;
        this.grant_type = grant_type;
        this.client_secret = client_secret;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGrant_type() {
        return grant_type;
    }

    public void setGrant_type(String grant_type) {
        this.grant_type = grant_type;
    }

    public String getClient_secret() {
        return client_secret;
    }

    public void setClient_secret(String client_secret) {
        this.client_secret = client_secret;
    }

}
