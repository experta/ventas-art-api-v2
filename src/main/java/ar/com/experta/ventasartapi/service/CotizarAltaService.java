package ar.com.experta.ventasartapi.service;

import ar.com.experta.ventasartapi.controller.response.CotizacionAltaResponse;
import ar.com.experta.ventasartapi.controller.response.CotizacionPremioResponse;
import ar.com.experta.ventasartapi.dao.impl.CotizarAltaDaoImpl;
import org.apache.http.HttpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CotizarAltaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AlicuotaService.class);

    @Autowired
    protected CotizarAltaDaoImpl cotizarAltaDaoImpl;
    protected CotizacionAltaResponse cotizacionAltaResponse;

    public CotizacionPremioResponse cotizacionAltaResponse(CotizacionPremioResponse in )   throws HttpException{
        try{
            LOGGER.info("ART:  - alta -  [{}]", in.getRazonSocial());
            CotizacionAltaResponse cotizacionAltaResponse = cotizarAltaDaoImpl.alta(in);
            if (cotizacionAltaResponse.getRetorno()!= null && cotizacionAltaResponse.getRetorno().equalsIgnoreCase("success")) {
                LOGGER.info("ART:  - alta OK");
                //popula CotizacionPremioResponse

                in.setNumeroSolicitud(cotizacionAltaResponse.getNroSolicitud());
                in.setRetorno(cotizacionAltaResponse.getRetorno());
                in.setErrorMsg("");

            }else{
                LOGGER.info("ART:  - alta fallida");
                in.setRetorno(cotizacionAltaResponse.getRetorno() != null?cotizacionAltaResponse.getRetorno():"Error al imprimir retorno");
                in.setErrorMsg(cotizacionAltaResponse.getErrorMsg() != null?cotizacionAltaResponse.getErrorMsg():"Error al imprimir error msg");
                throw  new HttpException("ART:  - alta fallida");
            }

        } catch (Exception e){
            LOGGER.info("ART:  - ERROR: en alta de cotizacion: [{}] disponibilidad: [{}], error al obtener la empresa: ", in.getRazonSocial(), in.getArtDisponibilidad(), in.getErrorMsg());
            in.setRetorno("error");
            in.setErrorMsg("Error al ejecutar el procedure");
            throw new HttpException("Error al ejecutar el procedure " + e.getMessage() );
        }

        return in;
    }



}
