package ar.com.experta.ventasartapi.service;

import ar.com.experta.ventasartapi.controller.response.CotizacionPremioResponse;
import ar.com.experta.ventasartapi.dao.impl.CotizarDetalleDaoImpl;
import ar.com.experta.ventasartapi.utils.ApiUtils;
import org.apache.http.HttpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;


@Service
public class DetalleCotizacionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DetalleCotizacionService.class);

    @Autowired
    protected CotizarDetalleDaoImpl dotizarDetalleDaoImpl;
    protected CotizacionPremioResponse cotizacionPremioResponse;

    protected CotizacionPremioResponse cotizacionDetalle (String nroSolicitud) throws HttpException {
        try{
            LOGGER.trace("ART:  - buscar detalle - cuit [{}]", nroSolicitud);
            Calendar currentDate = Calendar.getInstance(); //Get the current date
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy/MM/dd"); //format it as per your requirement
            ApiUtils apiUtils = new ApiUtils();

            cotizacionPremioResponse = dotizarDetalleDaoImpl.buscarDetalleCotizacion(nroSolicitud);
            cotizacionPremioResponse.setNumeroSolicitud(nroSolicitud);
            cotizacionPremioResponse.setFechaInicioVigencia((formatter.format(currentDate.getTime())));
            cotizacionPremioResponse.setDatosInformados(cotizacionPremioResponse.armarListaDatosInformados(
                    cotizacionPremioResponse.getCiiu(),
                    cotizacionPremioResponse.getCiiuDesc(),
                    cotizacionPremioResponse.getCapitas(),
                    cotizacionPremioResponse.getMasaSalarial(),
                    apiUtils.validateDisponibilidad(cotizacionPremioResponse.getArtDisponibilidad(),cotizacionPremioResponse.getArtActual(),"")));
            cotizacionPremioResponse.setNuestrasAlicutas(cotizacionPremioResponse.armarListaAlicuotas(
                    cotizacionPremioResponse.getAlicuotaFija(),
                    cotizacionPremioResponse.getAlicuotaVariable(),
                    cotizacionPremioResponse.getTotalCuotaMensual().toString(),
                    cotizacionPremioResponse.getTotalCuotaAnual().toString()));
            if (cotizacionPremioResponse.getRetorno()!= null && cotizacionPremioResponse.getRetorno().equalsIgnoreCase("success") ) {
                LOGGER.trace("ART:  - buscar detalle - OK");
            }else{
                LOGGER.trace("ART:  - buscar detalle - Fallido");
                throw new HttpException("ART:  - buscar detalle - Fallido");
            }
        } catch (Exception e){
            LOGGER.trace("ART:  - ERROR: buscar cotizacion - nro solicitud: [{}] empresa: [{}], error ", nroSolicitud);
            throw new HttpException(e.getMessage());
        }

        return cotizacionPremioResponse;
    }

}
