package ar.com.experta.ventasartapi.service;

import ar.com.experta.ventasartapi.controller.response.CotizacionEmpresaResponse;
import ar.com.experta.ventasartapi.dao.impl.CotizarDaoImpl;
import org.apache.http.HttpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class EmpresaService {

    public static final String PERFIL_DEFAULT_ESTUDIO = "USER_ESTUDIO";
    private static final Logger LOGGER = LoggerFactory.getLogger(EmpresaService.class);

    @Autowired
    private CotizarDaoImpl cotizarDaoImpl ;


    protected CotizacionEmpresaResponse buscarEmpresa (String cuit) throws HttpException {
        CotizacionEmpresaResponse cotizacionResponse = new CotizacionEmpresaResponse();
        try{
            LOGGER.trace("ART:  - buscar empresa - cuit [{}]", cuit);
            cotizacionResponse = cotizarDaoImpl.empresaCheck(cuit);
        } catch (Exception e){
            throw  new HttpException("ART:  - buscar empresa - cuit [{}], error al obtener la empresa  "+e.getMessage());

        }

        if ((cotizacionResponse.getRetorno() != null) && cotizacionResponse.getRetorno().equals("success")) {
            LOGGER.trace("ART:  - buscar empresa - OK");

        }else{
            LOGGER.trace("ART:  - buscar empresa - OK".concat(cotizacionResponse.getErrorMsg()!= null ? cotizacionResponse.getErrorMsg() : "Error al querer obtener SRT"));
        }

        return cotizacionResponse;
        }

}