package ar.com.experta.ventasartapi.service;

import ar.com.experta.ventasartapi.controller.response.CotizacionEmpresaResponse;
import ar.com.experta.ventasartapi.controller.response.CotizacionPremio;
import ar.com.experta.ventasartapi.controller.response.CotizacionPremioResponse;
import org.apache.http.HttpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class ArtService {
    @Autowired
    private EmpresaService empresaService;

    @Autowired
    private AlicuotaService alicuotaService;

    @Autowired
    private CotizarAltaService cotizarAltaService;

    @Autowired
    private DetalleCotizacionService detalleCotizacionService;

    public CotizacionPremioResponse obtenerAlicuotaArt(String cuit, String masaSalarial, String capitas, String actividad, String vendedor, String origen, String alicuotaFijaActual, String alicuotaVariableActual ) throws HttpException {

        CotizacionPremioResponse cotizacionPremioResponse = new CotizacionPremioResponse();

        if (Integer.valueOf(capitas) > 50){
            cotizacionPremioResponse.setRetorno("error");
            cotizacionPremioResponse.setErrorMsg("Las capitas no pueden superar los 50 empleados.");
            throw new HttpException("Las capitas no pueden superar los 50 empleados.");
        }

        CotizacionEmpresaResponse cotizacionEmpresaResponse = empresaService.buscarEmpresa(cuit);

        if (cotizacionEmpresaResponse.getRetorno()!=null && cotizacionEmpresaResponse.getRetorno().equals("errors_to_front_end")){
            cotizacionPremioResponse.setRetorno(cotizacionEmpresaResponse.getRetorno());
            cotizacionPremioResponse.setErrorMsg(cotizacionEmpresaResponse.getErrorMsg());
            throw new HttpException(cotizacionEmpresaResponse.getErrorMsg());
//        }else if ( cotizacionEmpresaResponse.getClienteExpertaArt() != null &&
//                cotizacionEmpresaResponse.getRegistradoAfip().equalsIgnoreCase("FALSE")){
//            cotizacionPremioResponse.setRetorno(cotizacionEmpresaResponse.getRetorno());
//            cotizacionPremioResponse.setErrorMsg("Cliente no registrado en AFIP");
//            throw new HttpException("Cliente no registrado en AFIP");
        }else if ( cotizacionEmpresaResponse.getClienteExpertaArt() != null &&
                cotizacionEmpresaResponse.getClienteExpertaArt().equalsIgnoreCase("TRUE")){
            cotizacionPremioResponse.setRetorno(cotizacionEmpresaResponse.getRetorno());
            cotizacionPremioResponse.setErrorMsg("Cliente de Experta ART");
            throw new HttpException("Cliente de Experta ART");
        }

        cotizacionPremioResponse.setCuit(cuit );
        cotizacionPremioResponse.setProductor(vendedor);
        cotizacionPremioResponse.setCiiu(actividad);
        cotizacionPremioResponse.setAlicuotaFijaActual(new BigDecimal(alicuotaFijaActual));
        cotizacionPremioResponse.setAlicuotaVariableActual(new BigDecimal(alicuotaVariableActual));

        //consultar alicuota get_precio_art
        CotizacionPremio cotizacionPremio =   alicuotaService.buscarAlicuota(cotizacionEmpresaResponse,cuit, masaSalarial, capitas,  actividad, vendedor, origen, alicuotaFijaActual, alicuotaVariableActual );
        if ((cotizacionPremio.getErrorMsg()!=null && cotizacionPremio.getErrorMsg().equalsIgnoreCase("") )){
            popularCampos(cotizacionPremioResponse,cotizacionEmpresaResponse,cotizacionPremio,masaSalarial, capitas);
        }else {
            cotizacionPremioResponse.setRetorno(cotizacionPremio.getRetorno());
            if (cotizacionPremio.getErrorMsg() != null && cotizacionPremio.getErrorMsg().equals("NOT_FOUND"))
                cotizacionPremioResponse.setErrorMsg("No hay una alicuota definida para los datos ingresados. Por favor contactese con su ejecutivo.");
            else if (cotizacionPremio.getErrorMsg() != null && cotizacionPremio.getErrorMsg().equals("CIIU_IVALID"))
                cotizacionPremioResponse.setErrorMsg("La actividad de la empresa es inválida. Por favor contactese con su ejecutivo.");
            else
                cotizacionPremioResponse.setErrorMsg("Hubo un error cotizando el producto");
        }

        return cotizacionPremioResponse;
    }

    public CotizacionPremioResponse cotizarArt(CotizacionPremioResponse cotizacionRequest) throws HttpException {
        //cotizar Alta
        return cotizarAltaService.cotizacionAltaResponse(cotizacionRequest);
    }

    public CotizacionPremioResponse cotizarDetalle(String cotizacionId) throws HttpException {
        //cotizar obtener detalle
        return detalleCotizacionService.cotizacionDetalle(cotizacionId);
    }

    protected CotizacionPremioResponse popularCampos(CotizacionPremioResponse cotizacionPremioResponse, CotizacionEmpresaResponse cotizacionEmpresaResponse, CotizacionPremio cotizacionPremio, String masaSalarial, String capitas){
        cotizacionPremioResponse.setRegistradoAfip(cotizacionEmpresaResponse.getRegistradoAfip());
        cotizacionPremioResponse.setClienteExpertaArt(cotizacionEmpresaResponse.getClienteExpertaArt());
        cotizacionPremioResponse.setClienteExpertaSCVO(cotizacionEmpresaResponse.getClienteExpertaSCVO());
        cotizacionPremioResponse.setRazonSocial(cotizacionEmpresaResponse.getRazonSocial());
        cotizacionPremioResponse.setArtActual(cotizacionEmpresaResponse.getArtActual());
        cotizacionPremioResponse.setAlicuotaVarMax(cotizacionPremio.getAlicuotaVariableMaxima());
        cotizacionPremioResponse.setArtDisponibilidad(cotizacionEmpresaResponse.getArtDisponibilidad() != null?
                cotizacionEmpresaResponse.getArtDisponibilidad() : "No obtuvo  Disponibilidad");
        cotizacionPremioResponse.setArtAdhesion(cotizacionEmpresaResponse.getArtAdhesion());
        cotizacionPremioResponse.setArtPermanenciaMin(cotizacionEmpresaResponse.getArtPermanenciaMin());
        cotizacionPremioResponse.setArtOmisionPago(cotizacionEmpresaResponse.getArtOmisionPago());
        cotizacionPremioResponse.setArtRescindida(cotizacionEmpresaResponse.getArtRescindida());
        cotizacionPremioResponse.setFechaVencimiento(cotizacionPremio.getVencimiento());
        cotizacionPremioResponse.setMasaSalarial(cotizacionEmpresaResponse.getMasaSalarial()==null? masaSalarial :
                cotizacionEmpresaResponse.getArtRescindida().equals("N")? masaSalarial :cotizacionEmpresaResponse.getMasaSalarial().toString());
        cotizacionPremioResponse.setCapitas(cotizacionEmpresaResponse.getCapitas()!=null? capitas :
                cotizacionEmpresaResponse.getArtRescindida().equals("N")? capitas : cotizacionEmpresaResponse.getCapitas());
        cotizacionPremioResponse.setProvincia(cotizacionEmpresaResponse.getProvincia());
        cotizacionPremioResponse.setProvinciaDesc(cotizacionEmpresaResponse.getProvinciaDesc());
        cotizacionPremioResponse.setLocalidad(cotizacionEmpresaResponse.getLocalidad());
        cotizacionPremioResponse.setLocalidadDesc(cotizacionEmpresaResponse.getLocalidadDesc());
        cotizacionPremioResponse.setZona(cotizacionEmpresaResponse.getZona());
        cotizacionPremioResponse.setAlicuotaFija(       cotizacionPremio.getAlicuotaFija());
        cotizacionPremioResponse.setAlicuotaVariable(   cotizacionPremio.getAlicuotaVariable());
        cotizacionPremioResponse.setAlicuotaVarMin(     cotizacionPremio.getAlicuotaVariableMinima());
        cotizacionPremioResponse.setRetorno(            cotizacionPremio.getRetorno());
        cotizacionPremioResponse.setErrorMsg(           cotizacionPremio.getErrorMsg());
        return cotizacionPremioResponse;
    }
}