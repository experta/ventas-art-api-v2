package ar.com.experta.ventasartapi.service;

import ar.com.experta.ventasartapi.controller.response.CotizacionEmpresaResponse;
import ar.com.experta.ventasartapi.controller.response.CotizacionPremio;
import ar.com.experta.ventasartapi.dao.impl.AlicuotaDaoImpl;
import org.apache.http.HttpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class AlicuotaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AlicuotaService.class);

    @Autowired
    protected AlicuotaDaoImpl alicuotaDaoImpl;
    protected CotizacionPremio cotizacionPrecio;

    protected CotizacionPremio buscarAlicuota (CotizacionEmpresaResponse in, String cuit, String masaSalarial, String capitas, String actividad, String vendedor, String origen, String alicuotaFijaActual, String alicuotaVariableActual ) throws HttpException {
        try{

            //buscar el perfil
            String perfilVendedor = alicuotaDaoImpl.buscarPerfil(vendedor);


            cotizacionPrecio = alicuotaDaoImpl.getAlicuota(in, cuit,  masaSalarial, capitas,actividad, perfilVendedor, origen, alicuotaFijaActual, alicuotaVariableActual );
        } catch (Exception e){
            throw  new HttpException( e.getMessage());

              }

        if (cotizacionPrecio.getErrorMsg() !=null && cotizacionPrecio.getErrorMsg().equalsIgnoreCase("")) {
            LOGGER.trace("ART:  - buscar alicuota - OK");
        }else{
            LOGGER.trace("ART:  - buscar empresa - Not OK");
            throw  new HttpException( "ART:  - buscar empresa - Not OK");
            //cotizacionPrecio.setErrorMsg("Error al ejecutar servicio para obtener alicuota");
        }

        return cotizacionPrecio;
    }



}
