package ar.com.experta.ventasartapi.utils;

import ar.com.experta.ventasartapi.config.Constantes;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class ApiUtils {

    private final static Integer CANTIDAD_CARACTERES_PREVIOS_MENSAJE_ERROR = 10;
    private final static Integer CANTIDAD_CARACTERES_POSTERIORES_MENSAJE_ERROR = 3;

    public String extractErrorMessage(String completeErrorMessage) {
        if (StringUtils.isNotEmpty(completeErrorMessage)) {
            if (completeErrorMessage.contains("message")) {
                return completeErrorMessage
                        .substring(completeErrorMessage.lastIndexOf("message") + CANTIDAD_CARACTERES_PREVIOS_MENSAJE_ERROR,
                                completeErrorMessage.length() - CANTIDAD_CARACTERES_POSTERIORES_MENSAJE_ERROR);
            }
        }
        return completeErrorMessage;
    }

    public String validateDisponibilidad(String disponibilidad, String artActual , String rescindida) {
        String afiliacion = "";
        if (disponibilidad!=null) {
            if (disponibilidad.equalsIgnoreCase(Constantes.TIPO_AFILIACION_ALTA)) {
                afiliacion = Constantes.SET_ALTA;
            } else if (disponibilidad.equalsIgnoreCase(Constantes.TIPO_AFILIACION_TRASPASO)){
                afiliacion = Constantes.SET_TRASPASO;
            }
        } else if (artActual.equals("FALSE") && rescindida.equals("N")){
            afiliacion = Constantes.SET_TRASPASO;
        }
        return afiliacion;
    }
}
