package ar.com.experta.ventasartapi.client;


import ar.com.experta.ventasartapi.controller.request.RequestKeycloakLogin;
import ar.com.experta.ventasartapi.dto.UsuarioKeycloak;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class KeycloakClient {


    @Value("${keycloak.auth-server-url}")
    private String hostKeyCloack;
    private static final String URI_COTIZAR = "/auth/realms/seguros/protocol/openid-connect/token";

    @Value("${kcclientid}")
    private String clientId;

    @Value("${kcsecret}")
    private String kcsecret;

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    private static ObjectMapper objectMapper = new ObjectMapper();

    public UsuarioKeycloak login(RequestKeycloakLogin requestKeycloakLogin) throws JsonProcessingException {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(hostKeyCloack + URI_COTIZAR);

        MultiValueMap<String, String> header = new LinkedMultiValueMap<String, String>();
        header.add("Content-Type", "application/x-www-form-urlencoded");

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("client_id",clientId);
        map.add("username",requestKeycloakLogin.getUsername());
        map.add("password",requestKeycloakLogin.getPassword());
        map.add("grant_type","password");
        map.add("client_secret",kcsecret);


        UsuarioKeycloak responseBodyJSON = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.POST,
                new HttpEntity<>(map,header),
                UsuarioKeycloak.class).getBody();


            return responseBodyJSON;
        }



    public UsuarioKeycloak refreshToken(String requestRefreshTokenKeycloak) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(hostKeyCloack + URI_COTIZAR);

        MultiValueMap<String, String> header = new LinkedMultiValueMap<String, String>();
        header.add("Content-Type", "application/x-www-form-urlencoded");
        //   String responseBodyJSON = restTemplate.postForObject(hostKeyCloack + URI_COTIZAR,requestKeycloakLogin,String.class);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("client_id","art-ventas-api");
        map.add("refresh_token",requestRefreshTokenKeycloak);

        map.add("grant_type","refresh_token");
        map.add("client_secret",kcsecret);

        UsuarioKeycloak responseBodyJSON = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.POST,
                new HttpEntity<>(map,header),
                UsuarioKeycloak.class).getBody();


        //     UsuarioKeycloak usuarioKeycloak = objectMapper.readValue(responseBodyJSON, UsuarioKeycloak.class);

        return responseBodyJSON;

    }

}
