package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;

@ApiModel
public enum Banco {

    AMERICAN_EXPRESS_BANK("295"),
    BANCO_CETELEM("331"),
    BANCO_CMF("319"),
    BANCO_COINAG("431"),
    BANCO_COLUMBIA("389"),
    BANCO_COMAFI("299"),
    BANCO_CREDICOOP("191"),
    BANCO_DE_CORRIENTES("094"),
    BANCO_DE_FORMOSA("315"),
    BANCO_DE_GALICIA("007"),
    BANCO_DE_INVERSION_Y_COMERCIO_EXTERIOR("300"),
    BANCO_DE_LA_CIUDAD_DE_BUENOS_AIRES("029"),
    BANCO_DE_LA_NACION_ARGENTINA("011"),
    BANCO_DE_LA_PAMPA("093"),
    BANCO_DE_LA_PROVINCIA_DE_BUENOS_AIRES("014"),
    BANCO_DE_LA_PROVINCIA_DE_CORDOBA("020"),
    BANCO_DE_LA_REPUBLICA_ORIENTAL_DEL_URUGUAY("269"),
    BANCO_DE_SAN_JUAN("045"),
    BANCO_DE_SANTA_CRUZ("086"),
    BANCO_DE_SANTIAGO_DEL_ESTERO("321"),
    BANCO_DE_SERVICIOS_FINANCIEROS("332"),
    BANCO_DE_SERVICIOS_Y_TRANSACCIONES("338"),
    BANCO_DE_VALORES("198"),
    BANCO_DEL_CHUBUT("083"),
    BANCO_DEL_SOL("310"),
    BANCO_DEL_TUCUMAN("060"),
    BANCO_DO_BRASIL("046"),
    BANCO_FINANSUR("303"),
    BANCO_HIPOTECARIO("044"),
    BANCO_INDUSTRIAL("322"),
    BANCO_INTERFINANZAS("147"),
    BANCO_ITAU_ARGENTINA("259"),
    BANCO_JULIO("305"),
    BANCO_MACRO("285"),
    BANCO_MARIVA("254"),
    BANCO_MASVENTAS("341"),
    BANCO_MERIDIAN("281"),
    BANCO_MUNICIPAL_DE_ROSARIO("065"),
    BANCO_PATAGONIA("034"),
    BANCO_PIANO("301"),
    BANCO_PROVINCIA_DE_TIERRA_DEL_FUEGO("268"),
    BANCO_PROVINCIA_DEL_NEUQUÉN("097"),
    BANCO_ROELA("247"),
    BANCO_SAENZ("277"),
    BANCO_SANTANDER_RIO("072"),
    BANCO_SUPERVIELLE("027"),
    BANK_OF_AMERICA_NATIONAL_ASSOCIATION("262"),
    BBVA_BANCO_FRANCES("017"),
    BNP_PARIBAS("266"),
    CITIBANK("016"),
    DEUTSCHE_BANK("325"),
    HSBC_BANK_ARGENTINA("150"),
    ICBC("015"),
    JPMORGAN("165"),
    MBA_LAZARD("312"),
    NUEVO_BANCO_DE_ENTRE_RÍOS("386"),
    NUEVO_BANCO_DE_LA_RIOJA("309"),
    NUEVO_BANCO_DE_SANTA_FE("330"),
    NUEVO_BANCO_DEL_CHACO("311"),
    RCI_BANQUE("339"),
    THE_BANK_OF_TOKYO_MITSUBISHI("018"),
    THE_ROYAL_BANK_OF_SCOTLAND("005");

    private static Map<String, Banco> map = new HashMap<>();

    static {
        for (Banco value : Banco.values()) {
            map.put(value.codigo(), value);
        }
    }

    private String codigo;

    private Banco (final String codigo) {
        this.codigo = codigo;
    }

    public static Banco fromCodigo(final String codigo) {
        return map.get(codigo);
    }

    public String codigo() {
        return codigo;
    }

}
