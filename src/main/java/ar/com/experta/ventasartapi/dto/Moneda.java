package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;

@ApiModel
public enum Moneda {

    PESOS(""),
    DOLARES("D"),
    QUINTALES(" ");

    private static Map<String, Moneda> map = new HashMap<>();

    static {
        for (Moneda value : Moneda.values()) {
            map.put(value.codigo(), value);
        }
    }

    private String codigo;

    private Moneda(final String codigo) {
        this.codigo = codigo;
    }

    public static Moneda fromCodigo(final String codigo) {
        return map.get(codigo);
    }

    public String codigo() {
        return codigo;
    }
}
