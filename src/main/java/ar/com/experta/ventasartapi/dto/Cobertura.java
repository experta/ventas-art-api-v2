package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

import javax.validation.constraints.NotNull;

@ApiModel
public class Cobertura {

    @NotNull (message = "Ingrese tipo de cobertura")
    private TipoCobertura tipoCobertura;

    public Cobertura() {
    }

    public Cobertura(TipoCobertura tipoCobertura) {
        this.tipoCobertura = tipoCobertura;
    }

    public TipoCobertura getTipoCobertura() {
        return tipoCobertura;
    }

    public void setTipoCobertura(TipoCobertura tipoCobertura) {
        this.tipoCobertura = tipoCobertura;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cobertura cobertura = (Cobertura) o;
        return tipoCobertura == cobertura.tipoCobertura;
    }

}

