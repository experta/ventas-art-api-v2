package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;

@ApiModel
public enum EstadoEmision {
    EMITIDA("E"),
    PENDIENTE("P");

    private static Map<String, EstadoEmision> map = new HashMap<>();

    static {
        for (EstadoEmision value : EstadoEmision.values()) {
            map.put(value.codigo(), value);
        }
    }

    private String codigo;

    private EstadoEmision(final String codigo) {
        this.codigo = codigo;
    }

    public static EstadoEmision fromCodigo(final String codigo) {
        return map.get(codigo);
    }

    public String codigo() {
        return codigo;
    }
}