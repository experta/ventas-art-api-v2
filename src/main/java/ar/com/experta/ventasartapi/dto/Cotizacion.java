package ar.com.experta.ventasartapi.dto;

import java.math.BigDecimal;

public class Cotizacion {

    private Integer numero;
    private BigDecimal prima;
    private BigDecimal impuestos;
    private BigDecimal recargoFinanciero;
    private BigDecimal recargoAdministrativo;
    private BigDecimal derechoEmision;
    private BigDecimal premio;

    public Cotizacion() {
    }

    public Cotizacion(Integer numero, BigDecimal prima, BigDecimal impuestos, BigDecimal recargoFinanciero, BigDecimal recargoAdministrativo, BigDecimal derechoEmision, BigDecimal premio) {
        this.numero = numero;
        this.prima = prima;
        this.impuestos = impuestos;
        this.recargoFinanciero = recargoFinanciero;
        this.recargoAdministrativo = recargoAdministrativo;
        this.derechoEmision = derechoEmision;
        this.premio = premio;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public BigDecimal getPrima() {
        return prima;
    }

    public void setPrima(BigDecimal prima) {
        this.prima = prima;
    }

    public BigDecimal getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(BigDecimal impuestos) {
        this.impuestos = impuestos;
    }

    public BigDecimal getPremio() {
        return premio;
    }

    public void setPremio(BigDecimal premio) {
        this.premio = premio;
    }

    public BigDecimal getRecargoFinanciero() {
        return recargoFinanciero;
    }

    public void setRecargoFinanciero(BigDecimal recargoFinanciero) {
        this.recargoFinanciero = recargoFinanciero;
    }

    public BigDecimal getRecargoAdministrativo() {
        return recargoAdministrativo;
    }

    public void setRecargoAdministrativo(BigDecimal recargoAdministrativo) {
        this.recargoAdministrativo = recargoAdministrativo;
    }

    public BigDecimal getDerechoEmision() {
        return derechoEmision;
    }

    public void setDerechoEmision(BigDecimal derechoEmision) {
        this.derechoEmision = derechoEmision;
    }
}
