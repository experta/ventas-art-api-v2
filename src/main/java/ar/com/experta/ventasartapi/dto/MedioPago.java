package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;

@ApiModel
public enum MedioPago {

    CANJE("9"),
    TARJETA_CREDITO ("3"),
    DEBITO_AUTOMATICO ("4"),
    COBRANZA_MANUAL ("0");

    private static Map<String, MedioPago> mapCodigosGLM = new HashMap<>();

    static {
        for (MedioPago medioPago : MedioPago.values()) {
            mapCodigosGLM.put(medioPago.codigo, medioPago);
        }
    }

    private String codigo;

    private MedioPago(final String codigo) {
        this.codigo = codigo;
    }

    public static MedioPago fromValueGLM(final String value) {
        return mapCodigosGLM.get(value);
    }

    public String codigo () {
        return codigo;
    }

}
