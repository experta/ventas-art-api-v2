package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;

@ApiModel
public enum Cultivo {

    SOJA_1RA("1"),
    MAIZ_1RA("2"),
    SORGO("3"),
    GIRASOL("4"),
    TRIGO_NORTE("5"),
    CEBADA_NORTE("6"),
    ALGODON("7"),
    TRIGO_SUR("8"),
    CEBADA_SUR("9"),
    CENTENO_NORTE("10"),
    CENTENO_SUR("11"),
    AVENA_NORTE("12"),
    AVENA_SUR("13"),
    GARBANZO("14"),
    SOJA_2DA("15"),
    MAIZ_2DA("16");

    private static Map<String, Cultivo> map = new HashMap<>();

    static {
        for (Cultivo value : Cultivo.values()) {
            map.put(value.codigo(), value);
        }
    }

    private String codigo;

    private Cultivo(final String codigo) {
        this.codigo = codigo;
    }

    public static Cultivo fromCodigo(final String codigo) {
        return map.get(codigo);
    }

    public String codigo() {
        return codigo;
    }

}