package ar.com.experta.ventasartapi.dto;

public class UsuarioArtApi {

    private String clientSecret;
    private String clientId;
    private String grantType;
    private String grantTypeRefresh;

    public UsuarioArtApi() {
    }

    public UsuarioArtApi(String clientSecret, String clientId, String grantType, String grantTypeRefresh) {
        this.clientSecret = clientSecret;
        this.clientId = clientId;
        this.grantType = grantType;
        this.grantTypeRefresh = grantTypeRefresh;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGrantType() {
        return grantType;
    }

    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    public String getGrantTypeRefresh() {
        return grantTypeRefresh;
    }

    public void setGrantTypeRefresh(String grantTypeRefresh) {
        this.grantTypeRefresh = grantTypeRefresh;
    }
}
