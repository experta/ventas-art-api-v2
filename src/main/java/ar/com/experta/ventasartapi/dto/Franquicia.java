package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

@ApiModel
public enum Franquicia {

    NO_DEDUCIBLE_6_PORCIENTO ("10", "20", "60"),
    DEDUCIBLE_5_PORCIENTO ("11","21", "80"),
    DEDUCIBLE_10_PORCIENTO("12","22", "90"),
    DEDUCIBLE_15_PORCIENTO("13","23", "100");

    private String codigoTradicional;
    private String codigoVerano;
    private String codigoGranizoResiembra40Porciento;


    Franquicia(String codigoTradicional, String codigoVerano, String codigoGranizoResiembra40Porciento) {
        this.codigoTradicional = codigoTradicional;
        this.codigoVerano = codigoVerano;
        this.codigoGranizoResiembra40Porciento = codigoGranizoResiembra40Porciento;
    }

    public String codigoVerano() {
        return codigoVerano;
    }
    public String codigoTradicional() {
        return codigoTradicional;
    }
    public String codigoGranizoResiembra40Porciento() {
        return codigoGranizoResiembra40Porciento;
    }

}
