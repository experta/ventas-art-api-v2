package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;

@ApiModel
public enum Nacionalidad {

    ALEMANIA("2"),
    ARGENTINA("3"),
    AUSTRALIA("4"),
    BOLIVIA("5"),
    BRASIL("6"),
    CANADA("7"),
    CHILE("8"),
    CHINA("9"),
    COLOMBIA("10"),
    COSTA_RICA("11"),
    ECUADOR("12"),
    ESPANIA("13"),
    FRANCIA("14"),
    HONDURAS("15"),
    ITALIA("16"),
    JAPON("17"),
    MEXICO("18"),
    NICARAGUA("19"),
    PARAGUAY("20"),
    PERU("21"),
    PANAMA("22"),
    EL_SALVADOR("23"),
    SUECIA("24"),
    SUIZA("25"),
    INGLATERRA("26"),
    URUGUAY("27"),
    ESTADOS_UNIDOS("28"),
    VENEZUELA("29"),
    CUBA("30"),
    SIRIA("31"),
    PORTUGAL("32"),
    DOMINICANA("33"),
    UGANDA ("34"),
    PUERTO_RICO("35"),
    MADAGASCAR("36"),
    RUMANIA("37"),
    OTRO("1");

    private static Map<String, Nacionalidad> map = new HashMap<>();

    static {
        for (Nacionalidad value : Nacionalidad.values()) {
            map.put(value.codigo(), value);
        }
    }

    private String codigo;

    private Nacionalidad(final String codigo) {
        this.codigo = codigo;
    }

    public static Nacionalidad fromCodigo(final String codigo) {
        return map.get(codigo);
    }

    public String codigo() {
        return codigo;
    }

}