package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;

@ApiModel
public enum TipoCobertura {

    VERANO("-"),
    GRANIZO_RESIEMBRA_20_PORCIENTO("-"),
    GRANIZO_RESIEMBRA_40_PORCIENTO("-"),
    HELADA("30"),
    VIENTO_FUERTE("40"),
    PLANCHADO("50");

    private static Map<String, TipoCobertura> map = new HashMap<>();

    static {
        for (TipoCobertura value : TipoCobertura.values()) {
            map.put(value.codigo(), value);
        }
    }

    private String codigo;

    private TipoCobertura(final String codigo) {
        this.codigo = codigo;
    }

    public static TipoCobertura fromCodigo(final String codigo) {
        return map.get(codigo);
    }

    public String codigo() {
        return codigo;
    }

}