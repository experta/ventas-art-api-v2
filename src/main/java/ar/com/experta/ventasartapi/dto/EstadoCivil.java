package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;

@ApiModel
public enum EstadoCivil {

    CASADO("1"),
    DIVORCIADO("2"),
    SEPARADO("3"),
    SOLTERO("4"),
    VIUDO("5"),
    CONVIVENCIAL("6");

    private static Map<String, EstadoCivil> map = new HashMap<>();

    static {
        for (EstadoCivil value : EstadoCivil.values()) {
            map.put(value.codigo(), value);
        }
    }

    private String codigo;

    private EstadoCivil(final String codigo) {
        this.codigo = codigo;
    }

    public static EstadoCivil fromCodigo(final String codigo) {
        return map.get(codigo);
    }

    public String codigo() {
        return codigo;
    }

}