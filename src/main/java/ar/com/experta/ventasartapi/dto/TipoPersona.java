package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;

@ApiModel
public enum TipoPersona {

    FISICA("1"),
    JURIDICA("2");

    private static Map<String, TipoPersona> map = new HashMap<>();

    static {
        for (TipoPersona value : TipoPersona.values()) {
            map.put(value.codigo(), value);
        }
    }

    private String codigo;

    private TipoPersona(final String codigo) {
        this.codigo = codigo;
    }

    public static TipoPersona fromCodigo(final String codigo) {
        return map.get(codigo);
    }

    public String codigo() {
        return codigo;
    }

}