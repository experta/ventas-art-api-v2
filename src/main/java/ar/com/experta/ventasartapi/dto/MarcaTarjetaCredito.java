package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;

@ApiModel
public enum MarcaTarjetaCredito {
    GALICIA_RURAL("GALIR"),
    VISA("VISAA");

    private static Map<String, MarcaTarjetaCredito> mapCodigosGLM = new HashMap<>();

    static {
        for (MarcaTarjetaCredito tipoPago : MarcaTarjetaCredito.values()) {
            mapCodigosGLM.put(tipoPago.codigo, tipoPago);
        }
    }

    private String codigo;

    private MarcaTarjetaCredito(final String codigo) {
        this.codigo = codigo;
    }

    public static MarcaTarjetaCredito fromValueGLM(final String value) {
        return mapCodigosGLM.get(value);
    }

    public String codigo () {
        return codigo;
    }
}
