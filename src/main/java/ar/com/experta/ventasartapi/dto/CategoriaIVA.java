package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;

@ApiModel
public enum CategoriaIVA {

    EXCENTO("4"),
    MONOTRIBUTISTA("6"),
    RESPONSABLE_INSCRIPTO("1");

    private static Map<String, CategoriaIVA> map = new HashMap<>();

    static {
        for (CategoriaIVA value : CategoriaIVA.values()) {
            map.put(value.codigo(), value);
        }
    }

    private String codigo;

    private CategoriaIVA(final String codigo) {
        this.codigo = codigo;
    }

    public static CategoriaIVA fromCodigo(final String codigo) {
        return map.get(codigo);
    }

    public String codigo() {
        return codigo;
    }

}