package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

@ApiModel
public class EmisionConsultaResponse {

    private String poliza;
    private String razonSocial;
    private String mailTomador;
    private String fechaEmision;

    public EmisionConsultaResponse() {
    }

    public EmisionConsultaResponse(String poliza, String razonSocial, String mailTomador, String fechaEmision) {
        this.poliza = poliza;
        this.razonSocial = razonSocial;
        this.mailTomador = mailTomador;
        this.fechaEmision = fechaEmision;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getPoliza() {
        return poliza;
    }

    public void setPoliza(String poliza) {
        this.poliza = poliza;
    }

    public String getMailTomador() {
        return mailTomador;
    }

    public void setMailTomador(String mailTomador) {
        this.mailTomador = mailTomador;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }
}
