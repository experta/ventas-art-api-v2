package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;

@ApiModel
public enum TipoDocumento {

    DNI("96"),
    CUIT("80"),
    CUIL("86"),
    LIBRETA_CIVICA("90"),
    LIBRETA_ENROLAMIENTO("91"),
    DOCUMENTO_PARA_EXTRANJEROS("92");

    private static Map<String, TipoDocumento> map = new HashMap<>();

    static {
        for (TipoDocumento value : TipoDocumento.values()) {
            map.put(value.codigo(), value);
        }
    }

    private String codigo;

    private TipoDocumento(final String codigo) {
        this.codigo = codigo;
    }

    public static TipoDocumento fromCodigo(final String codigo) {
        return map.get(codigo);
    }

    public String codigo() {
        return codigo;
    }

}