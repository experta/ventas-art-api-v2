package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

@ApiModel
public class PersonaExpuestaPoliticamente {

    private String cargo;
    private String organismo;
    private String relacion;

    public PersonaExpuestaPoliticamente() {
    }

    public PersonaExpuestaPoliticamente(String cargo, String organismo, String relacion) {
        this.cargo = cargo;
        this.organismo = organismo;
        this.relacion = relacion;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getOrganismo() {
        return organismo;
    }

    public void setOrganismo(String organismo) {
        this.organismo = organismo;
    }

    public String getRelacion() {
        return relacion;
    }

    public void setRelacion(String relacion) {
        this.relacion = relacion;
    }
}
