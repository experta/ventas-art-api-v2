package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;

@ApiModel
public enum Sexo {

    MASCULINO("1"),
    FEMENINO("2");

    private static Map<String, Sexo> map = new HashMap<>();

    static {
        for (Sexo value : Sexo.values()) {
            map.put(value.codigo(), value);
        }
    }

    private String codigo;

    private Sexo(final String codigo) {
        this.codigo = codigo;
    }

    public static Sexo fromCodigo(final String codigo) {
        return map.get(codigo);
    }

    public String codigo() {
        return codigo;
    }

}