package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;

@ApiModel
public enum Provincia {

    SALTA("A"),
    BUENOS_AIRES("B"),
    CABA("C"),
    SAN_LUIS("D"),
    ENTRE_RIOS("E"),
    LA_RIOJA("F"),
    SANTIAGO_DEL_ESTERO("G"),
    CHACO("H"),
    SAN_JUAN("J"),
    CATAMARCA("K"),
    LA_PAMPA("L"),
    MENDOZA("M"),
    MISIONES("N"),
    FORMOSA("P"),
    NEUQUEN("Q"),
    RIO_NEGRO("R"),
    SANTA_FE("S"),
    TUCUMAN("T"),
    CHUBUT("U"),
    TIERRA_DEL_FUEGO("V"),
    CORRIENTES("W"),
    CORDOBA("X"),
    JUJUY("Y"),
    SANTA_CRUZ("Z");

    private static Map<String, Provincia> map = new HashMap<>();

    static {
        for (Provincia value : Provincia.values()) {
            map.put(value.codigo(), value);
        }
    }

    private String codigo;

    private Provincia(final String codigo) {
        this.codigo = codigo;
    }

    public static Provincia fromCodigo(final String codigo) {
        return map.get(codigo);
    }

    public String codigo() {
        return codigo;
    }

}