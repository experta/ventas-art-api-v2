package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;


@ApiModel
public enum CategoriaIIBB {

    INSCRIPTO_LOCAL("I0"),
    CONVENIO_MULTILATERAL("I1"),
    EXENTO("I2"),
    NO_INSCRIPTO("I3"),
    REGIMEN_SIMPLIFICADO("I4");

    private static Map<String, CategoriaIIBB> map = new HashMap<>();

    static {
        for (CategoriaIIBB value : CategoriaIIBB.values()) {
            map.put(value.codigo(), value);
        }
    }

    private String codigo;

    private CategoriaIIBB(final String codigo) {
        this.codigo = codigo;
    }

    public static CategoriaIIBB fromCodigo(final String codigo) {
        return map.get(codigo);
    }

    public String codigo() {
        return codigo;
    }

}