package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

import java.math.BigDecimal;

@ApiModel
public class EmisionResponse {

    private EstadoEmision estado;
    private Integer solicitud;
    private BigDecimal prima;
    private BigDecimal impuestos;
    private BigDecimal recargoFinanciero;
    private BigDecimal recargoAdministrativo;
    private BigDecimal derechoEmision;
    private BigDecimal premio;
    private Integer poliza;

    public EmisionResponse() {
    }



    public EmisionResponse(EstadoEmision estado, Integer solicitud, BigDecimal prima, BigDecimal impuestos, BigDecimal recargoFinanciero, BigDecimal recargoAdministrativo, BigDecimal derechoEmision, BigDecimal premio, Integer poliza) {
        this.estado = estado;
        this.solicitud = solicitud;
        this.prima = prima;
        this.impuestos = impuestos;
        this.recargoFinanciero = recargoFinanciero;
        this.recargoAdministrativo = recargoAdministrativo;
        this.derechoEmision = derechoEmision;
        this.premio = premio;
        this.poliza = poliza;
    }

    public EstadoEmision getEstado() {
        return estado;
    }

    public void setEstado(EstadoEmision estado) {
        this.estado = estado;
    }

    public Integer getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Integer solicitud) {
        this.solicitud = solicitud;
    }

    public BigDecimal getPrima() {
        return prima;
    }

    public void setPrima(BigDecimal prima) {
        this.prima = prima;
    }

    public BigDecimal getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(BigDecimal impuestos) {
        this.impuestos = impuestos;
    }

    public BigDecimal getRecargoFinanciero() {
        return recargoFinanciero;
    }

    public void setRecargoFinanciero(BigDecimal recargoFinanciero) {
        this.recargoFinanciero = recargoFinanciero;
    }

    public BigDecimal getRecargoAdministrativo() {
        return recargoAdministrativo;
    }

    public void setRecargoAdministrativo(BigDecimal recargoAdministrativo) {
        this.recargoAdministrativo = recargoAdministrativo;
    }

    public BigDecimal getDerechoEmision() {
        return derechoEmision;
    }

    public void setDerechoEmision(BigDecimal derechoEmision) {
        this.derechoEmision = derechoEmision;
    }

    public BigDecimal getPremio() {
        return premio;
    }

    public void setPremio(BigDecimal premio) {
        this.premio = premio;
    }

    public Integer getPoliza() {
        return poliza;
    }

    public void setPoliza(Integer poliza) {
        this.poliza = poliza;
    }
}
