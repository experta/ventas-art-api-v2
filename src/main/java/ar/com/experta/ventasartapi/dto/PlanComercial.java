package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;

@ApiModel
public enum PlanComercial {

    TRADICIONAL_0_PORCIENTO("AGRO0",true),
    TRADICIONAL_1_PORCIENTO("AGRO1",true),
    TRADICIONAL_2_PORCIENTO("AGRO2",true),
    TRADICIONAL_3_PORCIENTO("AGRO3",true),
    TRADICIONAL_4_PORCIENTO("AGRO4",true),
    TRADICIONAL_5_PORCIENTO("AGRO5",true),
    TRADICIONAL_6_PORCIENTO("AGRO6",true),
    TRADICIONAL_7_PORCIENTO("AGRO7",true),
    TRADICIONAL_8_PORCIENTO("AGRO8",true),
    TRADICIONAL_9_PORCIENTO("AGRO9",true),
    TRADICIONAL_10_PORCIENTO("AGR10",true),
    TRADICIONAL_11_PORCIENTO("AGR11",true),
    TRADICIONAL_12_PORCIENTO("AGR12",true),
    TRADICIONAL_13_PORCIENTO("AGR13",true),
    TRADICIONAL_14_PORCIENTO("AGR14",true),
    TRADICIONAL_15_PORCIENTO("AGR15",true),
    VERANO_0_PORCIENTO("AGR20",false),
    VERANO_1_PORCIENTO("AGR21",false),
    VERANO_2_PORCIENTO("AGR22",false),
    VERANO_3_PORCIENTO("AGR23",false),
    VERANO_4_PORCIENTO("AGR24",false),
    VERANO_5_PORCIENTO("AGR25",false),
    VERANO_6_PORCIENTO("AGR26",false),
    VERANO_7_PORCIENTO("AGR27",false),
    VERANO_8_PORCIENTO("AGR28",false),
    VERANO_9_PORCIENTO("AGR29",false),
    VERANO_10_PORCIENTO("AGR30",false),
    VERANO_11_PORCIENTO("AGR31",false),
    VERANO_12_PORCIENTO("AGR32",false),
    VERANO_13_PORCIENTO("AGR33",false),
    VERANO_14_PORCIENTO("AGR34",false),
    VERANO_15_PORCIENTO("AGR35",false);


    private static Map<String, PlanComercial> map = new HashMap<>();

    static {
        for (PlanComercial value : PlanComercial.values()) {
            map.put(value.codigo(), value);
        }
    }
    private String codigo;
    private Boolean esTradicional;

    private PlanComercial(final String codigo, final Boolean esTradicional) {
        this.codigo = codigo;
        this.esTradicional = esTradicional;
    }

    public static PlanComercial fromCodigo(final String codigo) {
        return map.get(codigo);
    }

    public String codigo() {
        return codigo;
    }

    public Boolean esTradicional () { return esTradicional;}

}