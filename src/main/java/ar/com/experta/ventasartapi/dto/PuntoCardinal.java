package ar.com.experta.ventasartapi.dto;

import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;

@ApiModel
public enum PuntoCardinal {
    NORTE("N"),
    SUR("S"),
    ESTE("E"),
    OESTE("O");

    private static Map<String, PuntoCardinal> map = new HashMap<>();

    static {
        for (PuntoCardinal value : PuntoCardinal.values()) {
            map.put(value.codigo(), value);
        }
    }

    private String codigo;

    private PuntoCardinal(final String codigo) {
        this.codigo = codigo;
    }

    public static PuntoCardinal fromCodigo(final String codigo) {
        return map.get(codigo);
    }

    public String codigo() {
        return codigo;
    }

}
